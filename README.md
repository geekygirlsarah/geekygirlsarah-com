# geekygirlsarah.com

[![Netlify Status](https://api.netlify.com/api/v1/badges/6b9259db-5daa-4714-99ae-c5a627fc5fae/deploy-status)](https://app.netlify.com/sites/geekygirlsarah/deploys)

It's my new website! Exciting, huh?

It's built in Jekyll, a static site generator, and is based on the Minimal Mistakes theme available at 
https://mademistakes.com/work/minimal-mistakes-jekyll-theme/ . The site has been greatly modified from the original 
theme.

## How to Run It Locally

1) Install Ruby v2.6.2 (Win, Mac) or `ruby` and `ruby-dev` for Linux
1) Run `bundle` (if that errors, run `gem install bundle` first)
1) Run `jekyll serve`
1) Navigate to [http://localhost:4000](http://localhost:4000)

## How to Run It Locally (Production Mode)

1) Install Ruby v2.6.2 (Win, Mac) or `ruby` and `ruby-dev` for Linux
1) Run `bundle` (if that errors, run `gem install bundle` first)
1) For OSX/Linux, run `export JEKYLL_ENV=production`. For Windows, run `set JEKYLL_ENV=production`
1) Run `jekyll serve`
1) Navigate to [http://localhost:4000](http://localhost:4000)
1) Optionally clear out the variable with `export JEKYLL_ENV=""` or `set JEKYLL_ENV=`

## How It's Deployed

The code is stored on Gitlab. When pushes are made to `master`, Netlify gets a webhook call. It then pulls the site 
down and tries to build it. It then stores it on their CDN for fast delivery.

Presently geekygirlsarah.com, geekygirlsarah.dev, and sarahwithee.com all redirect to this site.

There are shortcut pages (like `/talks`) that do quick redirects to other pages (like `/speaking`). Part of it is to 
provide easy links to some resources so I don't have to share a longer URL (like `/pancreas` for 
`/speaking/building-an-open-source-artificial-pancreas/`). Some of this is for backward compatibility for the blog and 
search engines. These are stored in `/_redirects`.

## Questions?

Feel free to reach out via Twitter [@geekygirlsarah](https://twitter.com/geekygirlsara) or through the 
[Contact page](https://geekygirlsarah.com/contact/) on the website. 