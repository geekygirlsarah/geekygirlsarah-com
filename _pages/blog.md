---
title: "Blog"
layout: splash
permalink: /blog/
date: 2020-12-20T23:14:00-04:00
header:
  overlay_color: "#000"
  overlay_filter: "0.2"
  overlay_image: /assets/images/splash_bg_alterconf_2016.jpg
  caption: "Speaking at AlterConf Portland 2016 reading a quote on vulnerability by Brené Brown"
---

Every once in a while, I write out some thoughts and put them up here! My 55+ blog posts can range across a wide 
variety of topics, both professional and personal, both technical and non-technical.

When I do blog, I tweet them out at [@geekygirlsarah](https://twitter.com/geekygirlsarah). You can subscribe to the 
[RSS feed](/feed.xml) as well. 

{% assign sorted_categories = site.categories | sort %}
View by Category: {% for category in sorted_categories %}
[{{ category[0] }}](/categories/{{ category[0] }}/){: .btn .btn--inverse .btn--large }{% endfor %}
{: .notice--primary}

{% assign postsByYear = site.posts | group_by_exp:"post", "post.date | date: '%Y'" %}
{% for year in postsByYear %}
<h2>{{ year.name }}</h2>
{% for post in year.items %}
<div class="blog_index_table_row">
  <div class="blog_index_table_cell_image">
{% if post.image %}
    <img src="{{- post.image | relative_url -}}" alt="Blog post thumbnail" class="blog-roll-image">
{% else %}
  {% assign postImage = "/assets/images/blog-writing.jpg" %}
    <img src="{{- postImage | relative_url -}}" alt="Blog post thumbnail" class="blog-roll-image">
{% endif %}
  </div>
  <div class="blog_index_table_cell_blurb">
  <h3><a href="{{ post.url }}">{{ post.title }}</a></h3> <em>({{ post.date | date_to_xmlschema | date_to_string: "ordinal", "US" }})</em><br />
    {% if post.excerpt %}
      {{ post.excerpt | strip_html}}
    {% else %}
      {{ post.content | strip_html | truncatewords:75 }} [...]
    {% endif %}
  </div>
</div>
<div>&nbsp;</div>
{% endfor %}
{% endfor %}
