---
title: "Contact"
layout: splash
permalink: /contact/
date: 2019-06-30T21:48:00-04:00
header:
  overlay_color: "#000"
  overlay_filter: "0.5"
  overlay_image: assets/images/splash_bg_build-and-beer_2016.jpg
  caption: "My team (and robot) at the Build and Beer bad robot building workshop held by Simone Giertz in 2016"
---

I want to hear from you! Whether it's feedback on something I worked on, wanting 
to collaborate on a project, or speaking at an event, I'd love for you to reach out.

I'm most easily accessible on Twitter [@geekygirlsarah](https://twitter.com/geekygirlsarah).

You're welcome to send me an email with the form below and I'll try to get back 
with you as soon as possible.

NOTE: I can NOT respond to requests to recruit, sell, or collaborate on things 
related to my day job. Please reach out through my employer if you are wishing to work 
with my team or me.

<form name="contact" method="post" data-netlify="true" netlify-honeypot="address">
  <p>
    <label>Your name*: <input type="text" name="name" required=""/></label>   
  </p>
  <p>
    <label>Your email*: <input type="email" name="email" required="" /></label>
  </p>
  <p>
    <label>Your phone: <input type="tel" name="phone" /></label>
  </p>
  <p class="hidden">
    <label>Don’t fill this out if you're human: <input name="address" /></label>
  </p>
  <p>
    <label>Why are you reaching out? (Speaking request, podcast guest, etc.): <select name="reason" >
      <option value="no_option_picked">[Please choose option]</option>
      <option value="spam">I'm sending an ad or spam</option>
      <option value="speaking">Conference/event speaking request</option>
      <option value="podcast">Podcast guest request</option>
      <option value="website">Website issue</option>
      <option value="collaboration">Collaborate on a project together (NOT related to my job)</option>
      <option value="hey">Just reaching out to say hi</option>
      <option value="other">Something else</option>
    </select></label>
  </p>
  <p>
    <label>Your message*: <textarea name="message" required=""></textarea></label>
  </p>
  <p>
    <button type="submit">Send</button>
  </p>
</form>
