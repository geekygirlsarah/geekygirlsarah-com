---
title: "Links"
layout: splash
permalink: /links/
date: 2021-01-01T80:47:00-04:00
header:
  overlay_color: "#000"
  overlay_filter: "0.5"
  overlay_image: /assets/images/splash_bg_robotteam_2013-1.jpg
  caption: "One of the first competition robots I helped program and build in 2013"
---

# Links

Here's where to find me:

[&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Mastodon &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;](https://mastodon.social/@geekygirlsarah){: .btn .btn--info .btn--large}
{: .text-center}

[&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Bluesky &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;](https://bsky.app/profile/geekygirlsarah.bsky.social){: .btn .btn--info .btn--large}
{: .text-center}

[&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Twitter &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;](https://twitter.com/geekygirlsarah){: .btn .btn--info .btn--large}
{: .text-center}

[&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; LinkedIn &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;](https://www.linkedin.com/in/sarahwithee/){: .btn .btn--info .btn--large}
{: .text-center}

[&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Gitlab &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;](https://gitlab.com/geekygirlsarah/){: .btn .btn--info .btn--large}
{: .text-center}

[&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; GitHub &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;](https://github.com/geekygirlsarah){: .btn .btn--info .btn--large}
{: .text-center}

[&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Twitch &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;](https://twitch.tv/geekygirlsarah){: .btn .btn--info .btn--large}
{: .text-center}

[&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Girls of Steel Robotics &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;](https://girlsofsteelrobotics.com/){: .btn .btn--info .btn--large}
{: .text-center}


# Lesser Used Links

I'm here, but really not active:

[&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Facebook &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;](https://www.facebook.com/geekygirlsarah){: .btn .btn--info .btn--large}
{: .text-center}

