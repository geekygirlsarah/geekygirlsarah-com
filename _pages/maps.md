---
title: "Maps"
layout: splash
permalink: /maps/
date: 2019-07-18T02:48:00-04:00
header:
  overlay_color: "#000"
  overlay_filter: "0.5"
  overlay_image: /assets/images/splash_bg_robotteam_2013-1.jpg
  caption: "One of the first competition robots I helped program and build in 2013"
---

In July 2019, I got curious about everywhere in the world I had been. I started looking over TripIt and through records 
of airlines I had flown on and cites I had been in, as well as looking over maps to track down places I had been. I've 
been able to put together the following page. (All are across my lifetime except the green icons, which are from 2015 
to present).

* ![Blue Icon](/assets/images/marker-icon-blue.png) - Cities lived in
* ![Green Icon](/assets/images/marker-icon-green.png) - Cities traveled to
* ![Orange Icon](/assets/images/marker-icon-orange.png) - Airports been in
* ![Red Icon](/assets/images/marker-icon-red.png) - Countries visited

<div id="mapid">To see the map, you need to enable Leaflet.js to load from unpkg.com and images/scripts from openstreetmap.org.</div>

<noscript>To see the map, you need to enable JavaScript and allow Leaflet.js to load from unpkg.com and images/scripts from openstreetmap.org.</noscript>
<script>
  // Set up map
  var myMap = L.map('mapid').setView([30, -50], 3);
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
  }).addTo(myMap);
  
  // Set up marker colors
  var blueIcon = new L.Icon({
    iconUrl: '/assets/images/marker-icon-blue.png',
    shadowUrl: '/assets/images/marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
  });
  var redIcon = new L.Icon({
    iconUrl: '/assets/images/marker-icon-red.png',
    shadowUrl: '/assets/images/marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
  });
  var greenIcon = new L.Icon({
    iconUrl: '/assets/images/marker-icon-green.png',
    shadowUrl: '/assets/images/marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
  });
  var orangeIcon = new L.Icon({
    iconUrl: '/assets/images/marker-icon-orange.png',
    shadowUrl: '/assets/images/marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
  });
  var yellowIcon = new L.Icon({
    iconUrl: '/assets/images/marker-icon-yellow.png',
    shadowUrl: '/assets/images/marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
  });
  var violetIcon = new L.Icon({
    iconUrl: '/assets/images/marker-icon-violet.png',
    shadowUrl: '/assets/images/marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
  });
  var greyIcon = new L.Icon({
    iconUrl: '/assets/images/marker-icon-grey.png',
    shadowUrl: '/assets/images/marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
  });
  var blackIcon = new L.Icon({
    iconUrl: '/assets/images/marker-icon-black.png',
    shadowUrl: '/assets/images/marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
  });
  
  
  // Add markers
  // (they're backward from listed at top to put blue on top, green next, then orange)

  // Countries
  {% for item in site.data.travels.countries %}
    L.marker([{{ item.latitude }}, {{ item.longitude }}], {icon: redIcon}).addTo(myMap).bindPopup("<b>Country I've Been To</b><br />{{ item.name }}");
  {% endfor %}  

  // Airports
  {% for item in site.data.travels.airports %}
    L.marker([{{ item.latitude }}, {{ item.longitude }}], {icon: orangeIcon}).addTo(myMap).bindPopup("<b>Airport I've Been To</b><br />{{ item.name }} - {{ item.code }} ");
  {% endfor %}  

  // Visited
  {% for item in site.data.travels.visited %}
    L.marker([{{ item.latitude }}, {{ item.longitude }}], {icon: greenIcon}).addTo(myMap).bindPopup("<b>City I've Visited</b><br />{{ item.name }}");
  {% endfor %}  

  // Homes
  {% for item in site.data.travels.lived %}
  L.marker([{{ item.latitude }}, {{ item.longitude }}], {icon: blueIcon} ).addTo(myMap).bindPopup("<b>City I've Lived In</b><br />{{ item.name }}<br />Years: {{ item.years }}");
  {% if item.radius %}
  var circle = L.circle([{{ item.latitude }}, {{ item.longitude }}], {
      icon: blueIcon,
      color: 'blue',
      fillColor: '#00f',
      fillOpacity: 0.2,
      radius: {{ item.radius }}
  }).addTo(myMap).bindPopup("<b>{{ item.name }}</b><br />Lived: {{ item.years }}");
  {% endif %}
  {% endfor %}  

</script>

Generated using Leaflet.js, OpenStreetMaps, and some creative use of Jekyll + a YML data file, Markdown, and JavaScript.