---
title: "Resume"
layout: splash
permalink: /resume/
date: 2023-05-22T19:16:00-05:00
header:
  overlay_color: "#000"
  overlay_filter: "0.2"
  overlay_image: /assets/images/splash_bg_ncwit_2018.jpg
  caption: "At a speed networking event at the NCWIT high school girls in computing awards ceremony in 2018"
---

_PDF copy is available upon request._

## Experience

### Software Engineering Consultant
_18F (General Services Administration), Remote (based in Washington, DC)_<br />
_Aug 2021 - Present_<br />

18F is a technology and design consultancy for the U.S. Government, inside the government. 18F partners with agencies to improve the user experience of government services by helping them build and buy technology. 

* Rebuilt and consolidated the 10+-year-old Federal Audit Clearinghouse from seven .Net services to one Django service as it was transferred from the Census Bureau to the GSA
* Conducted a tech discovery with the Department of State's Bureau of Educational and Cultural Affairs to determine how to modernize, reduce tech debt, and consolidate ten websites they own and maintain
* Develop and maintain several open source projects that 18F has created over its existence
*	Shared knowledge through speaking at engineering guild and all-hands meetings as well as other guilds or events
*	Lead the "Tech Talks" and "Someone Talks About Something" regularly occurring internal events, and founded the Team Coffee Editorial Board to develop and run the all-hands meetings (called Team Coffees)

### Lead Mentor
_Girls of Steel Robotics, Pittsburgh, PA_<br />
_Nov 2019 - Present_<br />

Girls of Steel Robotics is a FIRST Robotics team that was founded in the fall of 2010 at Carnegie Mellon University. It consists of about 50 students from 12 different schools across all grade levels. 
*	Support students from middle to high school as lead mentor of the FIRST Tech Challenge program
*	Manage the logistics and timelines to maintain the program
*	Supervise students as we take them to regional competitions
*	Have regular student support one-on-one check-ins with students
*	Answer technical and business questions related to students’ work towards competing
*	Support girls in high school as technical mentor for the FIRST Robotics Challenge program

### Open Source Maintainer and Developer
_Code Thesaurus, Remote_<br />
_Oct 2017 – Present_<br />

A polyglot developer reference tool to help you compare languages you know with ones you don’t for reference or learning. An open-source project six years in the making. Started over Hacktoberfests 2017-2020, now public with 120+ contributors.

*	Created, architected, designed, and coded the main project
*	Built robust documentation system for the project
*	Created automated testing CI/CD pipelines to ensure public contributions are more reliable
*	Find and document issues that need to be worked on, then support and answer questions that contributors have
*	Manage project roadmap towards implementing new features

### Senior Software Engineer
_Advanced Agrilytics, Remote (based in Indianapolis, IN)_<br />
_Jun 2020 - Jul 2021_<br />

Advanced Agrilytics provides growers with customized strategies to optimize their crop outputs. I was hired as one of the first two engineers employed to help take their code base created by outside contractors in-house.
*	Architected and implemented an automated end-of-year report system for growers that were previously manually generated from R scripts, reducing runtime by an order of magnitude from 100s to 10s of hours
*	Containerized and automated deployments for previously manually deployed services on Amazon Web Services (AWS)
*	Configured CI/CD deployment pipelines for all projects after moving from contractor’s Bitbucket to our GitLab repos
*	Introduced Agile methodologies used by the team to help develop good culture for the team and future developers

### Freelance/Contract Software Engineer
_Jun 2019 – May 2020_<br />

* Implemented caching and login systems with Redis backend
*	Fixed bugs and added new features with a small team on a React, React Native, and multiplatform C# app to help caregivers more efficiently report patient information to doctors
*	Enhanced academic papers search without needing to subscribe to journals using AWS in Python

### Software Developer Generalist, Data Pipeline team 
_Arcadia.io, Remote (based in Pittsburgh, PA)_<br />
_Feb 2018 – Feb 2019_<br />

Our team developed a system to replace an expensive third-party component to bring electronic healthcare records in-house nightly.
*	Inherited a Ruby on Rails project from a former team member; took ownership by learning the framework to add new features and fix security problems, as well as deploy it to AWS Fargate
*	Implemented features and fixed bugs in Monitoring and other microservices in Scala
*	Containerized monitoring services, then configured continuous integration systems to build and deploy the service automatically
*	Created Terraform automation to deploy monitoring, AWS Kinesis Firehose, and ElasticSearch services
*	Mentored the junior developer on the team and interviewed new candidates for the team



### Software Developer, Laboratory Information Management System team
_Stowers Institute for Medical Research, Kansas City, MO_<br />
_Jul 2016 – Nov 2017_<br />

On this team, I developed laboratory management software that more than 400 scientists in more than 20 science labs used in their daily research. The product was written in PHP and Symfony (backend) and JavaScript and ExtJS (frontend).
*	Collaborated with Tissue Culture lab members to rewrite the Tissue Culture module in the newer product frameworks, which with their regular input, helped make the module optimize their workflow and speed up their data entries of experiment results
*	Developed new features for the new Molecular Biology NGS module
*	Installed and configured a new daily documentation generator for the backend codebase


### Software Developer (three teams)
_Commerce Bank, Kansas City, MO_<br />
_Jun 2015 – July 2016_<br />

*	Architected and developed a team data metric reporting system using C#, ASP.net, and MVC, which is used by many departments
*	Extended the Branch Connections framework in C# to utilize a third-party loan document service, which saved money and time needed to keep up with latest federal and local loan laws
*	Created a real time fraud detection system using Hadoop, Spark, Python, Java, and R with a team at a company hackathon, which was estimated by the fraud prevention department to prevent up to $1.2 billion in fraudulent transactions


### Software Engineering Intern
_Lexmark, Lenexa, KS_<br />
_Summers 2013, 2014_<br />

* Design, architect, and develop a new automated testing framework for business intelligence reports using C++ and CPPUnit
*	Implemented new features for Perceptive Photo Transfer Client in C#, JavaScript, and jQuery
*	Mentored other interns to bring them up to speed on the main product and helped them debug errors

### _Undergraduate Teaching Assistant_
_University of Missouri-Kansas City, Kansas City, MO_<br />
_2012-2015_<br />

* Undergraduate teaching assistant for CS 201 (Intro to C++) starting with lab teaching (2012-2014) then lecture instructing (2014-2015) by developing assignments, grading student work, and ensuring ABET accreditation for the class

### Prior software development roles
_2005-2013_
* Developed websites in PHP and provided web hosting services through my own business
* Developed custom Windows scripts and websites in VBScript and Sharepoint for a tech support and consulting company
* Built and customized e-commerce websites in ASP and VBScript for multiple clients


## Volunteering and Projects

**Mentoring**

I donate my time to help mentor women in technology as well as future public speakers. I usually mentor 2 people at any given time.

**Non-Profit Work and Board of Directors Membership**

I have volunteered with non-profits, both on boards and off, since the mid-2000s. I was vice president of PFLAG-KC and treasurer of Prototype PGH in my past. I help run Meetups and conferences with Code & Supply Co.


## Event, Meetup, and Conference Speaking

I love sharing what I learn with the developer communities of all experience levels. I have been speaking, teaching, and mentoring in many forms since 2011. Besides local classes and workshops, I have spoken and keynoted at over 65 events locally, nationally, internationally, and virtually. I have organized multiple events and conferences as well. All of my notes, videos, and slides can be found at [geekygirlsarah.com/speaking](/speaking).

## Skills

**Knowledgeable (used in production):**

**Languages:** 	Python, JavaScript, PHP, C++, C#/ASP.net, Java, Scala, SQL, Ruby, HTML, CSS<br />
**Frameworks:**  Django, jQuery, Node.js, Express, Symfony, ExtJS, Twilio, Ruby on Rails, React, Jekyll<br />
**Cloud/CI/CD/DevOps:**  Heroku, AWS, CloudFoundry, Cloud.gov, GitHub Actions, GitLab CI, Travis CI, Azure, Terraform<br />
**Databases:** 	PostgreSQL, SQL Server, MySQL, Redis, SQLite<br />
**Version Control:**  Git, GitHub and GitLab, Subversion (SVN), Team Foundation Server <br />
**IDEs:**  JetBrains/IntelliJ suite, VS Code, Visual Studio, Eclipse<br />

**Learning/Experimenting With (not used in production):**
Cordova, Azure (IoT Hub, Service Queues), Xamarin, React Native, MongoDB/NoSQL

## Education
University of Missouri-Kansas City, Bachelor of Science in Computer Science
