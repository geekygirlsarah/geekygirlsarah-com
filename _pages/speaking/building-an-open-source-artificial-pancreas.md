---
title: "Building an Open Source Artificial Pancreas"
layout: single
# author: sarah
permalink: /speaking/building-an-open-source-artificial-pancreas/
date: 2020-10-18T19:30:00-04:00
description: Slides, videos, and more for my talk "Building an Open Source Artificial Pancreas"
# classes: wide
toc: true
toc_label: "Page Contents"
toc_icon: "file-alt"
author_profile: true
---

This page will always be updated with the most recent version of this talk. 

Full session: The latest version is 1.8 which was given at Pittsburgh Tech Fest on 08/12/2020.

Lightning Talk: The latest version is 1.3 which was given at !!Con on 5/11/2019.

## Slides/Code/Diagrams

[Slides (full, PDF)](https://github.com/geekygirlsarah/talk-building_open_source_artificial_pancreas/raw/master/Building%20an%20Open%20Source%20Artificial%20Pancreas%20(long%20session).pdf) 
[Slides (lightning talk)](https://slides.com/geekygirlsarah/i-built-an-artificial-pancreas#/live)

## Videos

Strange Loop (2019, 40 min): [https://www.youtube.com/watch?v=5prZU5RxkZ4](https://www.youtube.com/watch?v=5prZU5RxkZ4)
{% include video id="5prZU5RxkZ4" provider="youtube" %}

Older videos:
PyCon (2019, 30 min version): [https://www.youtube.com/watch?v=1kT1VoX7VAs](https://www.youtube.com/watch?v=1kT1VoX7VAs)
!!Con (2019, 10 min version): [https://www.youtube.com/watch?v=IgxhCTs4S3c](https://www.youtube.com/watch?v=IgxhCTs4S3c)


## History

Date       | Type            | Version | Venue
---------- | --------------- | ------- | ------
2018-08-17 | Lightning Talk  | 1.0     | Self.Conference
2018-09-27 | Lightning Talk  | 1.1     | Strange Loop
2019-01-09 | Lightning Talk  | 1.2     | Code Mash
2019-03-03 | Conf Session    | 1.0     | Penguicon
2019-03-04 | Conf Session    | 1.1     | PyCon US
2019-03-11 | Lightning Talk  | 1.3     | !!Con
2019-06-07 | Conf Session    | 1.2     | Self.Conference
2019-08-07 | Conf Session    | 1.3     | THAT Conference
2019-09-14 | Conf Session    | 1.4     | Strange Loop
2019-10-11 | Opening Keynote | 1.5     | DevSpace Technical Conference
2019-11-08 | Conf Session    | 1.6     | Missouri, Iowa, Nebraska and Kansas Celebration of Women in Computing 
2020-01-07 | Conf Session    | 1.7     | Code Mash
2020-08-13 | Conf Session    | 1.8     | Pittsburgh Tech Fest


## Abstract/Description

Have you ever thought about what open source software or hardware could achieve? What if it could help improve people's 
lives by solving some of their health problems?

After the medical tech industry kept promising a system to help automatically manage insulin for type 1 diabetic people 
and never delivering, some people got together to find ways to do it with the tech they already had. Over the past few 
years, a "closed-loop" system has been developed to algorithmically regulate people's insulin levels. After reverse 
engineering bluetooth sensors and 915 MHz insulin pumps, the system became possible. As a diabetic, I also built this 
system and saw my sugar values stabilize much more than I could ever achieve doing it manually myself. Now I'm working 
on contributing back to the projects as well.

I want to talk about this system, from a technical side as well as a personal side. You'll learn about OpenAPS (the 
open artificial pancreas system) and how it works, what problems it solves, and its safety and security concerns. 
You'll see how the system has helped me and what this means for my health now and in the future. Ultimately, you'll see 
how we, as software developers, can change people's lives through the code we write.

<!-- ## Talk Outline

...

## Transcript

...

-->