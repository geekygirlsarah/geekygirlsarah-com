---
title: "FIRST Ladies/Rosie Talks"
layout: single
# author: sarah
permalink: /speaking/first-ladies/
date: 2020-10-18T19:30:00-04:00
description: Slides, videos, and more for my FIRST Ladies/Rosie Talks talk
# classes: wide
toc: true
toc_label: "Page Contents"
toc_icon: "file-alt"
author_profile: true
---

This page will always be updated with the most recent version of this talk. 
The latest version is 1.1 which was given on 12/19/2021.

## Slides

[Slides (PDF)](https://geekygirlsarah.com/assets/images/GoS%20FIRST%20Ladies%20Talk%202021.pdf)

I'm working on a version with what I would say with each slide. Stay tuned!

## History

Date | Version | Venue
-----|---------|------
2021-09-25 | 1.0 | Girls of Steel Hackathon 
2021-12-19 | 1.1 | Rosie Talks Season 2

## Notes

Thank you all for coming! I hope you all were inspired by this talk and how I got to the place I'm at both in my career 
and outside too! Please feel free to reach out if you have any questions!