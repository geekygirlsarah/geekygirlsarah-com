---
title: "Lightning Talks"
layout: single
# author: sarah
permalink: /speaking/lightning-talks/
date: 2021-06-27T00:00:00-04:00
description: Slides for all of my lightning talks 
# classes: wide
toc: true
toc_label: "Page Contents"
toc_icon: "file-alt"
author_profile: true
---

These are the lightning talks I have given over the years. Since they’re shorter talks, I've put them all on this page instead of their own individual pages.

## Easily Building Web Crawlers for Data Gathering (and Fun)
December 20, 2021

[Slides](https://gitlab.com/geekygirlsarah/talk-web_crawlers)

## How a Pinball Machine Works
January 8, 2020, Code Mash, Sandusky, OH
September 13, 2019, Strange Loop, St. Louis, MO

[Slides](https://slides.com/geekygirlsarah/pinball)
{% include video id="HdkHzCSGgDI" provider="youtube" %}
[YouTube (Code Mash 2020)](https://www.youtube.com/watch?v=HdkHzCSGgDI)

## Building an Open Source Artificial Pancreas
This talk has been given so many times it now has its own page: [Building an Open Source Artificial Pancreas](/speaking/building-an-open-source-artificial-pancreas/)

## How to Start a Podcast
May 19, 2017, Self.Conference, Detroit, MI

5 minute lightning talk created within 30 minutes of presenting

[Slides](https://slides.com/geekygirlsarah/i-built-an-artificial-pancreas/)

## Why Self Care is Harder For Me
August 12, 2016, AndConf, Camp Meeker, CA (near San Francisco)

90 second lightning talk

[Blog Post](https://medium.com/@geekygirlsarah/why-self-care-is-hard-for-me-9c58c047dcd0)

## Encouraging Kids Through Robotics
May 20, 2016, Self.Conference, Detroit, MI

5 minute lightning talk created within 30 minutes of presenting

[Slides](https://github.com/geekygirlsarah/talk-encouraging_kids_through_robotics_mentoring)

## Why You Should Participate in a Hackathon
October 19, 2013, Missouri, Iowa, Nebraska and Kansas Celebration of Women in Computing, Kansas City, MO

5 minute lightning talk submitted and accepted as an undergraduate student

[Slides](https://github.com/geekygirlsarah/talk-minkwic_lightning_talks_2013/)

## Becoming an Undergraduate Teaching Assistant
October 19, 2013, Missouri, Iowa, Nebraska and Kansas Celebration of Women in Computing, Kansas City, MO

5 minute lightning talk submitted and accepted as an undergraduate student

[Slides](https://github.com/geekygirlsarah/talk-minkwic_lightning_talks_2013/)
