---
title: "Nevertheless, Sarah Withee Coded (2017 Edition)"
excerpt: "Happy International Women's Day! This is my annual 'Nevertheless, She Coded' post!"
date: 2017-03-08
permalink: /2017/03/08/nevertheless-she-coded/
author_profile: true
toc: false
categories:
  - Women in Computing
tags:
  - shecoded
---

## I began coding because...

my parents had a Commodore 64, and my dad taught my sister and I how to type in the command to run a game. I was interested in what the command meant, so I found the computer manual and looked it up. I found all sorts of other commands in the BASIC language, and learned them and started doing all sorts of things on it. From that time in first grade on, I found it fascinating. I still find cool programming challenges fascinating to this day.

## I'm currently hacking on...

a podcast to boost voices of women and other underrepresented groups in tech, as well as a site that will compare languages you know with ones you don't in order to quickly pick up a new language, or use as a reference if you don't know one that well.

## I'm excited about...

my current hacking projects, but also in general helping other women in the tech world.

## My advice for other women who code is...

that it's not always easy out there in the tech world. If you love it though, keep doing it in some form in your life. If you can do it at work, or on the side, or whatever, try to keep some form of involvement in tech. Also connect with a group of other women in tech who are diverse and inclusive. I recommend the [Women in Tech Slack](https://witchat.github.io). They've been one of the best, most supportive groups I've been a member of.