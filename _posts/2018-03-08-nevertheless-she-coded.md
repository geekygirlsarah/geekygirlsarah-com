---
title: "Nevertheless, Sarah Withee Coded (2018 Edition)"
excerpt: "Happy International Women's Day! This is my annual 'Nevertheless, She Coded' post!"
date: 2018-03-08
permalink: /2018/03/08/nevertheless-she-coded/
author_profile: true
toc: false
categories:
  - Women in Computing
tags:
  - shecoded
---

## I began/continue to code because...

<img src="/assets/images/2018/03/commodore64.gif" alt="Image of programming on the Commodore 64" />


My family had a Commodore 64 (old computer from the 80s) when I was growing up. I learned the commands to type on it to do things, and found it fascinating to build little programs to do things. And... it stuck. I got a degree in computer science and became a software engineer for my job.

I continue to code because I love problem solving and thinking through ways to get the computer to solve these things. It's fun to me and even after all these years, it is still something I enjoy doing and enjoy learning all sorts of new things about technologies.

## I recently overcame...

Job hunting! I spent about 8 months looking for a new job, but wanted to take my time and find something truly fulfilling. I also was looking for jobs outside of the city I've lived in all my life. I'm happy to say I found a job that has smart and thoughtful people, interesting and challenging work, great compensation, and in a cool city. I started in February 2018 and looking forward to seeing what I accomplish here. (I wrote a blog post about the job process I had as well as the nonsense of interviewing in tech here:  https://geekygirlsarah.com/2018/02/05/a-post-about-my-new-job-and-the-ones-i-didnt-get/ )

## I'm currently hacking on...

A project that will let polyglot developers quickly have a resource to compare languages they know and don't know so that they can transition languages easier and faster. It can also be a great quick reference for any developers too.

## I look up to...

Other people who support and empower women and non-binary people in tech, both those in the field and those trying to enter it.

## My advice for allies to support women and non-binary folks who code is....

Offer lots of positive encouragement. Donate your time to helping them get into the field. Signal boost their accomplishments. Spend some time researching good ways to be allies for underrepresented groups. Remind them that they are awesome and smart and amazing!

## My advice for other women and non-binary folks who code is...

Stay strong. The tech industry can be toxic, but if you love the work you do, try to keep pursing it.

And you can always ping me on social media (@geekygirlsarah everywhere) and let me know how I can help encourage and support you too!