---
title: 'Year in Review: Part 2 &#8211; 2019 Goals'
excerpt: "In Part 1 of my Year in Review post, I wrote about what happened in the past year (or two) in my life, both good things that happened as well as some things that could be improved. In this part, I hope to put into words what I really want to figure out and work on in this year."
date: 2019-05-19T17:59:53+00:00
permalink: /2019/05/19/year-in-review-part-2-2019-goals/
# canonical_url: "https://yoursite.com/custom-canonical-url"
# image: 
medium_post:
  - ""
categories:
  - Life Updates
tags:
  - job hunting
  - year in review
---

I started writing this post back in January (an ideal time to do some yearly reflections and goal-setting). Then life took over. My whole team was laid off, I started organizing Abstractions' program and CFP (call for proposals), and got a huge wave of conference acceptances. All of this took away time and sort of backlogged this post. So it's mid-May and this post got abandoned. Coincidentally, my idea of what I wanted out of the year also changed, so perhaps it worked out in the end to wait this long before I finish up this super old post.

In Part 1 of my Year in Review post, I wrote about what happened in the past year (or two) in my life, both good things that happened and some things that could be improved. In this part, I hope to put into words what I really want to figure out and work on in this year.

**Job Hunting**

At the end of February, my company laid off my whole team. I feel better knowing this wasn't anything to do with me, and got a small severance. But I am on the job hunt. It feels like I just went through this a little while ago, but it happens. 

It seems to be a weirder spot I find myself falling into with each job hunt. I'm an amazing generalist: I'm the Jill of all trades and the master of none. I rock at a lot of things that are thrown at me, even with lack of knowledge in it, but it's a hard sell to try to say &#8220;no really, I'm a senior (or higher) level developer despite not being an expert in that thing.&#8221; Companies seem to love hiring those experts. Don't get me wrong, I'd _love_ to be considered one of those. The problem is I keep landing in a variety of fields and are all totally different, languages that are all different, doing tasks that are all different. It really sets me up to be great at whatever I work on, but it's a harder sell to tell them to hire me. I think I'm really the perfect consultant but want the stability of a corporate workplace. 

Add in my conference speaking, workshop teachings, and all the other things I do and I look a lot more attractive to employers. But considering I have a bunch of time I'll need off for the future events I have, I become less attractive. Go figure. In this case, I'm probably better suited for developer advocacy, but I also love development too much to want to give it up.

Then people often say &#8220;You're SOOO good with people, you should be a manager.&#8221; Well, ok&#8230; I think I am good with people. I could manage a small team or project probably alright. The problem is that's not THE job position I want. I'll do it as a part of my regular work but I know I will feel very out of place and unfulfilled if I moved to that type of role.

So I'm still looking. I have many leads (which I will probably need all that I can get) and hopefully one of them sees my talent and is willing to take a chance on me, and they in turn are everything I'd want or need out of a job!

**Mentoring and Teaching**

I quit doing most of my mentoring and teaching last year, mostly after my move, and it left a bit of a hole in my heart. I want to get back into it. I'm thinking at minimum of going back to the kids in robotics that I helped out for several years. I may not be able to do it as regularly as usual (maybe 1 day a week instead of 2-3), but I think it's important enough I need to go back.

(Update: I did find a high school girls' FIRST robotics team. I applied to volunteer with them and a background check is still pending. I am thinking of applying to TEALS, an organization that works with local schools to help improve computer science classes and departments, including sometimes teaching the classes.)

**Conference Speaking**

Wow, conference speaking. In the last year, I have had a >90% acceptance rate (only turned down from one conference in the dozen or more I applied to). It's great, it's refreshing feeling this compared to past years' really terrible rejection rates and lack of new ideas. It's been harder to fit them in though. The great thing though is that the one I got rejected from would mostly be a nostalgia feel of speaking in the past and it going so well, but I don't know that there's much I miss outside of seeing some people I like again. (It's also a conference with a big focus on drinking culture, something that I don't partake in and found I don't miss from conferences that don't have that.) I've been going to so many cool conferences with cool aspects to them (either fun locations like being in a water park, or cool twists like it ends with a premiere of a movie, or are just so much about the joy of technology and not at all work-related) that I'd much rather keep going to those. So I'll probably be focusing on those in the future.

Not directly related to conference speaking, over at Abstractions (the conference I'm helping organize right now), we did some open mentoring CFP sessions. They were zoom video calls where people could come in, ask any conference-related, CFP-related, or talk-related question they had. We were asked about ideas they had, how to make better abstracts to submit, and just for general advice. I LOVED that we could provide this for them. So much so that I'm working towards making open CFP mentoring a regular event every month. So I'd love to get this up and going.

**Reading and Listening**

Like many people, I tend to build up a collection of browser tabs full of things I want to read or look at later. For years, I built up bookmarks and Pocket entries. And the books people would loan me to read piled up and I never read them. I just never sat myself down to really go through these things.

One thing I discovered last year was Pocket's text-to-speech feature. You could bookmark sites in it, and they'd show up in a simple easy-to-read format on screen, but if you clicked the headphone icon, it would read it to you. Amazing! Many mornings now I get up, open Pocket, and have it just start reading me things. I feel like I learn so much now, both in terms of cool things (like how they took the picture of the black hole) or current events (latest news and other things), but also can catch up with some friend's blogs. It's been really helpful, especially since I can listen in the car (though don't have a regular job to drive to anymore) and makes my mornings feel better than just showering and getting ready with nothing going on. 

I hope to be able to keep this up. I'm really a sponge when it comes to learning things, so I want to keep filling my life with great things. Maybe with this habit, I can finally start to figure out ways to read books (even if they're read to me). I always feel bad that I end up with books I don't touch (and some of them I've had with me for a decade now and don't remember the original owners). 

**Self Care**

The more time goes on, the more I realize this is important. I want to make sure I dedicate enough time to taking care of myself, but enough money too. One example is that I need a haircut, but also don't have a hair person in Pittsburgh yet. I really need to buckle down and get some recommendations. I'm kind of thinking maybe I need weekly or monthly self care goals and strive to meet them.

**Side Projects**

I've had a variety of things I've wanted to do over the last few years. Some have been touched, some not. I rarely get things finished, and sometimes I just run out of energy or time to work on a thing. It's kind of depressing. 

One of the things I think is most important is starting to find ways to finish them. One finished project is better than 12 unfinished ones. I need to find some and spend the time to finish them out.

Another thing I really want to do is to find collaborators. I can't do these on my own, and I know some people want to be involved in open source projects, find ways to boost their resumes, learn more things in tech, as well as ways to boost their own names, so I'd love to provide ways people can do that. It'd also be nice to work with some friends (current ones or maybe new). I'm not totally sure how to find these people yet, but maybe that's where social media (or perhaps this blog post) could come in.

**Other things**

There's some other things I'd like to figure out, but they seem all able to fall under the above categories. And I'd rather see this goals post go online than drag on and on about a hundred other things.

**Conclusion**

2019 has been a decent year so far, despite a few of the downsides. I think I have less concrete goals now than I have in the past, but perhaps finding some direction to go with things can help set me up for success in the future, whatever that &#8220;success&#8221; may look like.

So here's to the rest of 2019! May it be filled with good things and some better direction!

As always, I love to hear what others are up to. Do you have goals (even halfway through the year)? How are you coming along on those? Feel free to reach out on Twitter or any of the other social media things (I'm @geekygirlsarah everywhere).

_Thanks to Jamie H. for reviewing this post for me!_