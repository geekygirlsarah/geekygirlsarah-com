---
title: "Learning to Share Accomplishments: Part 1 - How to Share Accomplishments"
excerpt: "In my last post, I talked about my last round of job hunting. More specifically, I mention one of the pieces of feedback was on how I come off as a bit more of an \"individual contributor\" (someone who just takes tasks and does them) as opposed to someone who actually designs or engineers solutions (someone who takes a vague thing and maps how it will work, breaks it apart into smaller tasks, then can work on some of those)."
date: 2020-03-16T09:00:00-04:00
permalink: /2020/03/16/learning-to-share-accomplishments-1/
image: /assets/images/2020/03/cat-11.jpg
author_profile: true
toc: true
toc_label: "Post Contents"
toc_icon: "file-alt"
medium_post:
  - ""
categories:
  - Accomplishments
  - Job Hunting
tags:
  - 
---

This post is a part of a series on job hunting and job interviewing.
{: .notice--info}

<figure>
    <img src='/assets/images/2020/03/cat-11.jpg' alt='Until I figure out a better image' width="50%" height="50%" />
    <figcaption>Until I figure out a better image to put here...</figcaption>
</figure>

In [my last post](/2019/09/02/reflections-on-the-latest-round-of-job-hunting/), I talked about my last round of job 
hunting. More specifically, I mention one of the pieces of feedback was on how I come off as a bit more of an 
"individual contributor" (someone who just takes tasks and does them) as opposed to someone who actually designs or 
engineers solutions (someone who takes a vague thing and maps how it will work, breaks it apart into smaller 
tasks, then can work on some of those). 

After some thought, I realized this is because over time I've learned to give credit to those on my team for some of 
the things we've worked on. I think it feels better to have a team take credit (well, and take on the failures) of all 
aspects of a project. While this is important and I think every team should do this, it sucks for a technical job 
interviews. It doesn't work to "sell yourself" to a future employer.

For part 1 of this post, I want to spend some time thinking through how to talk about accomplishments. What's the right 
way to do it without feeling like I have an inflated ego? How do I own up to my work, even when I've been on teams?
Why does it feel so hard to do?

## "Bragging" About Yourself

As someone who now has been in dozens of conferences, podcasts, videos, websites, blogs/articles, etc., I've had to 
come to terms with tooting my own horn some. All of these things usually want a bio from me. It's a lot easier now that 
I have made multiples and keep them around for things like this. In the beginning, it wasn't easy. In fact it was just 
downright terrible. It just felt wrong almost to try to write about all these things. It felt like bragging.

The easiest way I found to toot my own horn? I found one rule makes it easy: **Stick to the facts.**

The basics are to look at things I actually accomplished factually. These things definitively are provable. They have 
a place, a time, a number, or just anything that is solid and concrete about them. These aren't disputable because 
they're not my opinions. I _did_ work that job. I _was_ on that team. I _wrote_ that code for _that_ project. Because 
I can't dispute these, it makes it easier to say about myself.

But the basics of a bio are easy. Actually taking a team accomplishment and diving into my specific thing is a bit 
harder (at least mentally).

For each project/team/accomplishment, what was my one specific contribution? In some cases this is easy, like when I 
implemented a feature for a project that made it into production. Other times less so, like when I built a thing and 
learned it later got scrapped. The scrapping wasn't my fault, and it feels weird to talk about it because it isn't 
visible anymore. It's still an accomplishment of mine though, and it's one of the things I'm trying to keep an open 
mind about when writing this post.

Finally, what specifics can I back up things with? Did something I build save the team time? Did it save the company 
money? Did it solve a major crisis? Did it fix a security flaw? These go back to the thing about adding more facts. If 
the company said "We think we can save this much money by running this in production", that's a thing I can definitely 
throw in the mix.

"But Sarah," you may say, "the impostor syndrome! I don't really deserve that credit! I wasn't the only one who worked 
on that thing!" (Or insert your favorite excuse here.) Tough cookies. Write it down anyway. Just because you weren't 
the only one that worked on it didn't mean _you_ didn't work on it. Keep that in mind! (And this is also where I need 
to take my own advice a bit more.)

Over time I think adding all these little things up helps make me sound awesome, even though individually maybe they 
don't. I don't think I'm born a level of more awesome than anyone else, I just happened to do a few cool things that, 
when bundled together in one narrative, have made some people say that I'm awesome. I really think anyone can probably 
do the same for their own interviews or bios.

## What I Personally Have Worked on in My Past Jobs

I wanted to spend some time looking back at my different jobs and the work I have done. I decided to put that into a 
separate post. Feel free to check that out [Part 2](../learning-to-share-accomplishments-2/).
 
## Conclusion

As I continue to interview at places, a cloud of insecurity has been starting to come over me. I've felt like because 
I haven't secured a job yet that maybe my career has been bad or maybe I'm just not as good as other people that are 
at the same point in their career as I am. I think figuring out these methods (and using it to write my 
accomplishments in [Part 2](../learning-to-share-accomplishments-2/)) has helped me see that I really do have a wide 
range of accomplishments and am good at the things I build. I hope maybe in the process of writing these two posts 
that I have also built up more confidence for my job interviews. Hopefully you've also learned a bit on how to write 
and talk about your accomplishments more confidently too.

I'd love to hear if anything about these posts resonated with you. Feel free to reach out!

~ Sarah
