---
title: "I Would Love to Be a Mother Someday But..."
excerpt: "I was a part of the \"cast\" of the Listen To Your Mother 2017 - Kansas City show with about a dozen others. We all read essays about our mothers, about motherhood, or about being a mom. I wrote an essay in 2013 and revised it for this show. Since it's Mother's Day and I haven't made this public before now, I decided to go ahead and share it. I can't post the video due to copyrights, but if you're a friend, you're welcome to ask to see it."
date: 2020-05-10T20:00:00-04:00
permalink: /2020/05/10/i-would-love-to-be-a-mother-someday-but/
image: /assets/images/2020/05/143LTYM.jpg
author_profile: true
toc: false
medium_post:
  - ""
categories:
  - Writings
  - Motherhood
tags:
  - 
---

I was a part of the "cast" of the 
[_Listen To Your Mother 2017 - Kansas City_ show](https://web.archive.org/web/20170613184122/http://listentoyourmothershow.com/kansascity/) 
with about a dozen others. We all read essays about our mothers, about motherhood, or about being a mom. I wrote an 
essay in 2013 and revised it for this show. Since it's Mother's Day and I haven't made this public before now, I decided 
to go ahead and share it. I can't post the video due to copyrights, but if you're a friend, you're welcome to ask to see 
it.
{: .notice--info}

<figure>
    <img src="/assets/images/2020/05/146LTYM.jpg" alt="The &quot;cast&quot; of the Listen to Your Mother Kansas City in front of 
    the theater we spoke at" />
    <figcaption>The "cast" of the Listen to Your Mother Kansas City in front of the Liberty Hall theater.</figcaption>
</figure>

I used to _**HATE**_ kids. I had NO intention of ever being a parent. Kids are annoying and gross and noisy and so much
work and…  ugh!  They did NOT appeal to me at ALL!

But then…  something happened. Somehow that motherly instinct switch flipped on. And all of a sudden…  kids are cute. 
And adorable. And sure, occasionally gross, but… I don’t know. I wanted one. I wanted to be a mom someday.

But becoming a mother is… difficult. And to explain why, I have to fill you in on some background story. 

For most of my life I’ve been confused. Depressed. Things just didn’t make a lot of sense. After many years of thought 
and discovery, I learned that I am transgender. You’ve probably heard the term a lot in the news lately. The basic 
nutshell is that for most people, the gender that they feel they are in their mind matches up with the sex of their 
body. For me it doesn’t. I very much have the mind of a woman. And my body? Well, it doesn’t quite match. But in 2007, 
at the age of 24, I started the process of taking hormones to start to fix things.

Does anyone out there remember puberty? That awkward time of your life when it felt like everything about your body 
changed? I had one when I was a young teen. I then got to do it again at 24. I call it “Puberty 2.0.” The confusion and 
depression started to lift out of my mind, and I started to actually like life, and even started liking myself. My body 
started changing, and all the weird pains and sensitivity that came with it. But overall life was getting better for me, 
and I was becoming happy with it. 

Then, about 6 months in…  _IT_ happened. I was out somewhere and I heard a kid cry. And I looked over. And I felt sad 
for the kid. I wondered what was wrong. I wanted to hold it. I wanted to comfort it.

**Oh crap.** I grew a motherly instinct. Now, not only do I want children, but I’ve had several people tell me I’m 
wonderful around children too. Something changed in me, and perhaps that’s not a bad thing.

But I couldn’t help but wonder that if I did have children, would they end up screwed up like me? Would they end up 
depressed or suicidal like I was? Would they be as confused about their gender as I was? I didn’t want to subject any 
child to any of those tortures, so that helped aid in my decision to not ever have children.  But maybe adopting would 
be fine. Maybe taking on a kid without my genetics would be better. They could have a good home, I could still be that 
family that they need.

To add more complication to the story, my parents disowned me about 9 years ago. They didn’t agree with my "lifestyle 
change," as some would call it. To anyone that has known me for a while though they can see a world of difference in my 
old self and current self. Still, it is a bit of a shame that my parents have decided that instead of having a happier 
(but slightly less “normal”) child, they would rather just have nothing to do with me. But this has made me wonder: if 
I adopt children, what would they ultimately think? I shouldn’t keep this information from them, no matter how 
successful my life is as a woman. But would they want to disown me? Would they freak out upon hearing this? Would they 
be angry at me? Would their thoughts of me change so that they couldn’t ever see me as a woman (or their mother) again? 

Even more difficult is probably my own struggles. It took well over 10 years for me to finally admit that I’m a woman. 
Will I struggle to actually call myself a mother? Is someone without a female body allowed to call themselves that? Will 
I ever be able to be in a relationship with a wonderful man that would not only love me for me (despite my anatomical 
flaws) but also be willing to adopt a child, or would I have to be a single mom on my own? 

The concept of a family is a bit confusing in my mind due to my past with my parents. But the more time goes on, the 
more I learn about myself, the more I become comfortable in my body, the idea of being a mother feels more right. It’s 
not the right time to have a kid right now. But maybe in a couple more years, I can adopt a child. And maybe that child 
will love that they will have a mom that cares that they exist. And maybe they can appreciate that I will love them as 
they are, and no matter how they turn out. And maybe they won’t be angry at me if I come out to them. 

And maybe, hopefully, becoming a mother someday won’t be as difficult and confusing as I used to think it would be. 


<figure>
    <img src="/assets/images/2020/05/143LTYM.jpg" alt="The curtain call after the show" />
    <figcaption>The curtain call at the end of the show.</figcaption>
</figure>
