---
title: "A Story About Growing Up in Kansas City"
excerpt: "I grew up in Kansas City. It's probably most famous for being that weird city in BOTH Kansas & Missouri. There's another interesting trait about it. The Missouri River cuts through KC too. I often heard about how South of the River was bad. Lots of crime. Drive-by shootings. etc. I was taught not to cross certain streets. It's all inherently based in racism and red-lining."
date: 2020-08-04T13:22:00-04:00
permalink: /2020/08/04/story-about-growing-up-in-kansas-city/
image: /assets/images/2020/08/osm-kansas-city.png
author_profile: true
toc: false
medium_post:
  - ""
categories:
  - Activism
  - Writings
tags:
  - 
---

<figure>
    <img src="/assets/images/2020/08/osm-kansas-city.png" alt="Map of Kansas City, KS/MO. Screenshot from OpenStreetMaps." />
    <figcaption>Map of Kansas City, KS/MO. Screenshot from 
      <a href="https://www.openstreetmap.org/search?query=kansas%20city%2C%20mo#map=11/39.1090/-94.6149" target="_blank" rel="noopener">OpenStreetMaps</a>.
    </figcaption>
</figure>


I took June/July off & only retweeted things related to Black Lives Matter & Pride/LGBTQ issues. I'm going back to my 
regular posting, but I wanted to share a story first.

I grew up in Kansas City. It's probably most famous for being that weird city in BOTH Kansas & Missouri. There's 
another interesting trait about it. The Missouri River cuts through KC too. Growing up I heard about "North of the 
River" (where I grew up) and "South of the River." I often heard about how South of the River was bad. Lots of crime. 
Drive-by shootings. etc.

As an adult, I ended up moving South of the River in KC. Turns out it's kind of a cool place to be! Lots of artsy 
things, diversity, and cool people. And sure, there's crime (I mean, there was North too) but it wasn't anything like I 
was given the impression. I had basically grown up in KC with the impression that certain areas of the city were "bad" 
because they had people of color, more "urban" neighborhoods, and other things. I was taught not to cross certain 
streets. It's all inherently based in racism and red-lining.

While I learned over time about these bad thoughts I grew up with, it took years to mentally rewire my brain. I'm 
extremely glad I moved, and in fact wanted to stay there before I moved to Pittsburgh. But I think it helped me grow as 
a person and I'm better because of it.

I think I realized moving South of the River in KC helped show me the many ways the city was set up to be worse for 
people of color. I saw how different parts of the city got less funding and got less care, but also how people 
struggled to leave due to costs of leaving.

This leads me to my main point of this thread:

Black Lives Matter and Pride aren't events, they're conversations. They aren't a one-time thing but they will continue 
on. They're movements, they're calls to action. And that action can't stop yet.

People right now are needlessly dying. Black, brown, & other people of color are dying from police and inconvenienced 
white people. LGBTQ people are still dying for who they love and are. US federal & state governments are killing many 
underprivileged people by COVID mismanagement.

We're hearing less about protests, more talks of police reform in some places, and some COVID numbers are going down. 
The focus on these things can't stop. The momentum has to keep going. We (especially white people, especially 
privileged people) can't just ignore this.

What can we do from here? Good question! I think there are several things, but I want to make a quick point:

"Privilege" is thrown around a lot, but it's not a bad thing on its own. It can be used for good or for bad. Key is to 
recognize it and see how it affects others. Privilege isn't a thing you have or a thing you don't. It's a scale. For 
example, I'm white (which gives some to me) but I also spent most of my adult life in poverty income (which takes some 
away).

Overall, it basically means what issues hold you back from success?

The more privilege you have, the better off you are, ensuring your safety, comfort, finances, & stability. The more you 
have, the more likely you don't realize you have it, & why awareness is key. It's why some say the system is against 
them when it's not against others.

So back to what can we do… there's several things!

1. Stop what you're doing and listen. Listen to black/brown/people of color, LGBTQ people, the less financially 
   fortunate, & others with less privilege. Their stories are important. Don't try to solve them, just listen. Listen 
   to the pain, sadness, frustration, & anger of Black people & how they can't beat the system. It's rarely their 
   fault, our systems are set up against them. Don't just hear, but truly listen. It's important. We all need to 
   understand where they're coming from.

2. Start following a diverse set of people on social media. People tend to associate with people like them (myself 
   included), but you can fix that. I'm working on it too. But make sure your feed is filled with lots of ideas, 
   opinions, and voices. It's good for you!

3. If you are able, join some protests. It's a hard time to do them with COVID, but if you're healthy, can stay distant, 
   & wear masks, they're great ways to help amplify voices & bring awareness to injustices in our government/laws. Big 
   groups boost voices of people who need it!

4. Donate! Send some extra money to non-profit organizations doing good work in your community. Every major city (and 
   some smaller too) have groups doing good work. Do some research, ask around. If you can't find any, consider NAACP, 
   ACLU, Black Lives Matter, or others. If you're in the tech industry like me, good chance you're easily in the top 
   10-15% of US incomes. Help some good organizations help other people out! Even if you're not in tech, simple $5-20 
   donations help a lot too!

5. Better yet, set up recurring donations. While something like $50 can help an org out, a monthly donation of $5 goes
   a lot further. Regular donations show your commitment to supporting them & helps ensure their financial stability 
   over time. It has a longer impact.

6. Can't do protests or donate money? It's ok, donate time! Ask any non-profit & that's another thing they're likely
   lacking. Find out how you can help them teach kids in underprivileged neighborhoods, provide services to those who
   need it, or distributing food. Or more!

7. If you have a smart phone (which >80% of the country does), keep your phone charged and ready when you go out. If 
   you're somewhere and the police show up, get your device out and start recording. It may not end badly, but better 
   safe than sorry. If it is bad, post it. Capturing bad police actions is one of the most important things that's 
   bringing awareness to the injustice that's happening in our country and the world. Posting it gives it that 
   awareness. Consider also sending to organizationss or lawyers, or tagging your lawmakers.

8. Consider discontinuing businesses that put underprivileged people at risk in the pandemic. Amazon, Instacart, 
   GrubHub, Uber, and others operate on the idea that they reduce customer costs by exploiting cheap labor. A pandemic 
   is NOT the time for this! However... There are people at high risk, esp the elderly, disabled, or people w/ chronic 
   health issues. They SHOULD use these, and if you're a low risk, let these services be in use for those people that 
   need them most. It's convenient for you, but necessary for them.

9. Finally, go find out your voting status. Check your city/county/state websites and see if you're registered. If not, 
   go register. If you are, find out how to vote absentee or with mail-in ballots. *All* states have this. Find out how 
   to do it to keep the polls reduced. Our elections coming up are some of the most important. Our governments on every 
   level are FILLED with people doing stuff with their own interests and don't care about representing you. Get people 
   into the offices that DO represent you! How? Well... Go to ballotpedia.org to learn what you're voting on and read 
   about your candidates. Write down who you support. Take that to the polls, or keep it for your absentee/mail-in 
   ballot. There's a lot of positions you haven't heard of, but they're important!

To put my money where my mouth is:

- Since starting my new tech job in June, I've been donating >$150/month to many non-profits, and plan to increase that 
  amount as I fix some finances.
- I'm registered to vote and will receive mail-in ballots until middle of next year.

If you made it this far, thanks! This needs to be a conversation. I want to keep learning. I want to hear the voices of 
the oppressed and do what I can to help boost them. I sincerely hope you do too. These conversations might be 
uncomfortable, but they're vitally important.

And finally, remember...

People are needlessly dying, but:

- The pandemic IS real
- LGBTQ lives DO matter
- Black lives DO matter

And we need to take care of our fellow humans.

The following is a direct copy from a [Twitter thread](https://twitter.com/geekygirlsarah/status/1290699532087496707) 
and [Facebook post](https://www.facebook.com/geekygirlsarah/posts/1702230929929018) I posted, both on August 4, 2020. 
There might be slightly typographic inconsistencies due to the formats of posting on Twitter and Facebook.
{: .notice--info}