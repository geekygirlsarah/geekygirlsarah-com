---
title: "Year in Review: Looking Back at 2020 and Looking Forward at 2021"
excerpt: I've tried to do a year-end retrospective for the past several years. Sometimes I pull it off (2015, 2018) and sometimes I don't (2016, 2019). I started to write a reflection and goals for 2019/2020 last year and wrote some of it on and off. By the time I got about halfway done, the Coronavirus pandemic hit in March and I basically lost the motivation. It's been a weird year.
date: 2021-01-01T15:00:00-04:00
permalink: /2021/01/01/2021-01-01-year-in-review/
image: /assets/images/2021/01/2020-2021.png
author_profile: true
toc: true
medium_post: ""
categories:
  - Life Updates
tags:
  - career
  - Code Thesaurus
  - finances
  - job hunting
  - live streaming
  - new job
  - open source
  - pandemic
  - public speaking
  - retrospective
  - year in review
---

<figure>
    <img src="/assets/images/2021/01/2020-2021.png" alt="</2020> <2021>" />
</figure>

I've tried to do a year-end retrospective for the past several years. Sometimes I pull it off (2015, 2018) and sometimes I don't (2016, 2019). I started to write a reflection and goals for 2019/2020 last year and wrote some of it on and off. By the time I got about halfway done, the Coronavirus pandemic hit in March and I basically lost the motivation.

It's been a weird year. While most people would say the year collectively sucked (and I'd 100% agree), it doesn't mean there weren't good things that happened in the year too. To make it all the more weird, some things are good and terrible mixed together.

My past reflections have been deep dives into the year. This year I don't really want to. There's too much to process and too many weird nuances to all of it. Instead, I want to summarize some thoughts I've had this past year.

So here we go...

## Summary Numbers In a Nutshell

* Gave 5 conference talks (2 physical, 3 virtual)
* Spoke at 4 meetups
* Organized 1 conference
* Wrote 4 blog posts
* Was a guest on 2 podcasts
* Did 10 PRs for [Hacktoberfest](https://hacktoberfest.digitalocean.com/)...
* ... which let me release 1 [open source project](https://codethesaur.us/) to the world...
* ... and got about 14 contributors to add code to it... 
* ... and developed it on about 50 hours of live streams on Twitch
* Donated over $1200 to nonprofits this year (the most I've ever donated)
* Rode my bike 272.3 miles (including a single day record 23.5 miles)
* Brought my A1c* down to 5.4%
* Lost about 25 pounds

 *Hemoglobin A1C is a blood test that determines the average blood glucose levels. Under 5.7% is normal, 5.7% to 6.4% is prediabetic, and 6.5% and higher is considered diabetic. Type 1 diabetics like myself are suggested to keep it under 7%. 

## People (or Lack Thereof)

This year had some very mixed good with bad things. I couldn't see most of my friends or go to any of my activities. As an [ambivert](https://www.merriam-webster.com/dictionary/ambivert) (a split introvert and extrovert), it's a weird situation. Some days I was perfectly fine stuck at home, and other days I had massive cases of cabin fever. What was sold as "maybe a few weeks" became 9 months of trying to stay at home as much as possible. I gave myself permission to go out for bicycling as much as I wanted to (which did include with some friends, and we went to the trails separately). I had two COVID scares when a friend came over (one in March, one in November, both were already in my "pod") then tested positive a few days later. Thankfully, I _appear_ to never have caught it, and as of July officially showed no antibodies. I did move in with a roommate in October. She's a friend of a mutual friend, and it's been nice having another human around.

I also saw, along with the rest of the world, massive uprisings from the movements for Black Lives Matter. I have known about the movement for several years, and known there was inherently still racism mixed in with our society, I tried to listen to stories, watch the videos (as gruesome as they were), read up on more issues, and even went to my first bike protest. (There's something amazing about an estimated 900 bicyclists taking to the streets causing chaos by doing nothing other than biking together in a massive group!) I have typically tried to stay away from politics on social media because I haven't wanted the trolls and negative attention, and have generally wanted to be an uplifting presence there. But in June and July, I just went quiet. I posted nothing new. I only retweeted things from people of color and about those issues. I added "#BlackLivesMatter" to my profiles and don't intend to take them off. And even though the movements have quieted down, I still am listening. I am still wanting to share stories. I am still trying to find more things I can do to support my friends of color and the people in my city. I am still donating money to causes to fight inequality and help those treated unfairly by the systemic issues of our society. I also want to continue to boost the stories of marginalized people and be a good ally. For all the horror that has happened to even cause these movements, I hope that the fact they're very publicly visible and the world is very aware of them now helps cause major change.

<figure>
  <a href="https://www.nytimes.com/2020/06/24/technology/facial-recognition-arrest.html" rel="noopener" target="_blank"><img src="/assets/images/2021/01/wrongfully_accused_by_an_algorithm.png" alt="Screenshot from a New York Times article 'Wrongfully Accused by an Algorithm'" /></a>
  <figcaption><a href="https://www.nytimes.com/2020/06/24/technology/facial-recognition-arrest.html" rel="noopener">Wrongfully Accused by an Algorithm</a> by Kashmir Hill, New York Times, June 24, 2020. This is one of dozens of articles I've read this year about well-intentioned technology having numerous severe consequences.</figcaption>
</figure>

Finally, the other major thing I think has come out of 2020 has been a _stronger_ perception of the tech industry and its continuing effect on people and society. While I still like working in this industry, I am growing more and more aware of how much the industry can actually make society worse and dramatically affect people's lives, often not for the better. I'm reminded of how enlightened I felt after taking my ethics class in college, only now I'm disheartened at how easy tech makes it for a few people to profit off of exploiting other people.  

Between the global pandemic, systemic racism, and the rise of unethical technology, there's not a lot one single person can do. However, I do hope that for what privileges I do have, I can always use them to have the greatest impact of good on the world. I learned a lot about these issues in the past several years, but learned even more about how bad they are in the past year and what I can do to help. I want to make 2021 a year I can continue to learn, grow, and improve as an ally, as well as continue to do what I can to try to at least my little corner of the world a better place. 

## Job and Career

I lost my regular job back in early 2019. While I did a lot of short-term contracts, I still looked for a full time job. The pandemic definitely made the struggle a bit worse, but in May 2020 I did find a job and started on June 1.

The interesting part of this particular job is that I'm one of the first two people hired to form a new engineering department. They had always used outside contractors and so the other engineer and myself formed a new team within a new department. We had so many conversations I've never had before about like thinking through software methodologies the team will use, what are our best practice standards, and what do we want our culture to be like? It's pretty amazing to be in on these conversations from the start. We did hire a third person too. There are definitely other downsides, like transitioning from contractors to employees working on code bases has been a bit rocky. It's definitely a lot of the challenge I had been looking for in an engineering job, and I feel incredibly lucky to work with the manager and teammates I do.

This is the first year that I have also seriously started considering pivots to my career. I've always loved being a developer and dealing with code and designing solutions, but after struggling to find a job, I did ponder other directions. Would I have an interest in being (and even be ready to be) a manager or team lead? Would I (or should I) go back and get a masters in computer science? For these questions and more, I pondered a lot but don't know that I have solid answers yet. I guess we'll see where 2021 leads me.

## Public Speaking and Conference Organizing

<figure>
    <img src="/assets/images/2021/01/code-mash-informercial-challenge.jpg" alt="I spoke at CodeMash conference doing the 'infomercial challenge', an improvised one-minute attempt to sell people on a randomly created product." />
  <figcaption>I spoke at CodeMash conference doing the 'infomercial challenge', an improvised one-minute attempt to sell people on a randomly created product. Photo: <a href="https://twitter.com/colindean/status/1215422516342161409/photo/1" rel="nofollow">Colin Dean</a>.</figcaption>
</figure>

I spoke at CodeMash in January, but told myself after that I didn't want to apply to any more CFPs except my top five conferences, and I would also consider invites. It was mostly after feeling like public speaking and travel has been a part of my life for several years, but I didn't really feel like it was benefiting me much anymore. Then the pandemic happened, which cancelled the invited places I was going to speak at and cancelled all the top 5 events I would have applied to. It sort of made that a bit easier. I ended up submitting to a local tech event in August, then was asked to submit something in October for a new event ran by a friend. I did end up speaking at a few events, but the lack of travel and the shift of not doing many events actually was a nice change that I appreciated. Even if 2021 opens up, I will probably limit my CFP applying to just my top favorites and possible invites.

After a _massive_ burnout with organizing [Abstractions conference](https://abstractions.io/), I reluctantly joined the [Heartifacts conference](https://heartifacts.codeandsupply.co/) team as an organizer. I helped run the CFP again and I think helped bring in numerous wonderful topics and speakers. The pandemic closed the world about the same time the CFP closed, which caused the organizing team a massive headache in trying to figure out how to pivot the conference. It wasn't perfect (both in general and as a virtual event), it is an event I am generally proud of. It also helps that I got to emcee it again (though virtually this time) and helped frame the event in a way to focus on the important mental health and community topics we had.

## Health

I hit my heaviest weight ever in June 2019, just before heading to speak at a conference in Oslo, Norway. While I don't want to obsess over certain numbers being "good" or "bad" as health is a very relative thing, I do want to say that I _felt_ the worse I have ever felt. I came back from Oslo having lost 5 pounds from so much walking and moving and tourism! I decided to try to focus on continuing to lose weight and improve my health.

<figure>
    <img src="/assets/images/2021/01/mapmyfitness_2020_weekly_stats.png" alt="A graph from MapMyFitness that shows 52 weeks of bicycling stats." />
  <figcaption>A graph from MapMyFitness that shows 52 weeks of bicycling stats.</figcaption>
</figure>

Over the course of 2019 and 2020, I tried to shift to eating differently as well as doing some exercising. I had been a regular on-again-off-again bicyclist, it wasn't too hard to pick it back up. I got a new bicycle after moving to Pittsburgh, and this year I pedaled over 272 miles! This combined with a better diet helped me lose 25 pounds, getting me back to a weight before I moved to Pittsburgh. While winter is always harder to get out on the bike, I'm hoping to keep the momentum up to help me continue to feel better, lose more weight, and go even further distances on the bicycle!

## Open Source and Live Streaming

<figure>
    <img src="/assets/images/2021/01/code_thesaurus_screenshot.png" alt="Screenshot from Code Thesaurus." />
  <figcaption>Screenshot from Code Thesaurus.</figcaption>
</figure>

I had been building Code Thesaurus in my mind for about five years now. I did the initial site in PHP and Laravel and committed the first part for [Hacktoberfest](https://hacktoberfest.digitalocean.com/) 2017. I made more small changes for Hacktoberfest 2018. For 2019, I probably made the biggest jump where the thing quit being just a sample layout to a _working_ sample layout, what I call its "functional prototype". Finally this year I actually made the biggest jump: I rewrote the whole thing from PHP and Laravel to Python and Django. [I told the world about my idea in September](https://twitter.com/geekygirlsarah/status/1307394319226155015), and marked the [repository](https://github.com/codethesaurus/) as accepting outside help for Hacktoberfest. I finally had a minimal version that was usable and merged my developer branch into production in mid-October! While the site progress has been slower since then for a few reasons, I have been working on it on and off again even when Hacktoberfest hasn't been happening.

So far, the positive reviews are great! People love the idea and want to use it. It needs a _lot_ more data added to it, and I hope to finish designing out pieces of it that allow more data to be added to it. I built it to be easy for people to contribute to it, no matter what programming language(s) they know, whatever their experience levels are, and whatever their open source experience may be. It's not perfect, but I'm pretty proud of how it's come along, how it's been built so far, and I look forward to being able to keep implementing all of the ideas in my head for it and build the tool I've always wanted!

This leads into how I built it this year. Last year I met [Sophie Déziel](https://twitter.com/sophiedeziel) at [!!con](http://bangbangcon.com/), another software engineer that has [live streamed her hardware and software builds on Twitch](https://twitch.tv/sophiedeziel) for a while. She encouraged me to try to also start live streaming my hardware and software builds. I also have known about [Suz Hinton](https://twitter.com/noopkat) who has a pretty strong presence doing [development on Twitch](https://twitch.tv/noopkat) as well. She's written several blog posts about streaming and her setup, but I really liked her ["Lessons from my first year of live coding on Twitch."](http://meow.noopkat.com/lessons-from-one-year-of-streaming-on-twitch/) Then this year I met [ChaelCodes](https://twitter.com/chaelcodes), another software engineer who has been regularly [streaming open source work](https://twitch.tv/chaelcodes) too. She gave me the push to not just stream but even announce Code Thesaurus early (something I was _extremely_ reluctant to do). Since mid-September, I've averaged about 5-7 hours a week [streaming my open source work](https://www.twitch.tv/geekygirlsarah) and the occasional video game. I stopped in December due to a work project sucking up all my time and energy, but I do hope to figure out a better schedule and start streaming again in 2021!

## Miscellaneous 2020 Goals

Aside from speaking less, I had a few other goals:

- Delete all accounts on the major, large tech company websites 
- Switch my phone to all open source software
- Delete all old email aliases at my old domain (sarahwithee.com) so I can give it away, preferably to another Sarah Withee
- Get rid of my last web hosting server

These were lofty goals, and I didn't accomplish them. I did make significant gains to completing them. I have managed to find many open source (and non-privacy invading) apps and delete some old ones, delete my Amazon and Microsoft accounts, migrate all data off of Google (except Google Voice), and delete about 70% of my old email aliases.

2021 will include trying to finish off these lists. I plan to blog in the future about my tech company deletion, software I replaced everything with, and steps to do it.

## Finances

At the beginning of 2020, I was still hurting financially from losing the full-time job. The contracts were helping, but I was still dipping into savings. I found I definitely acknowledge how lucky and privileged I am to have not only found a job both doing something I love, but also managing to dso it in the midst of the global pandemic. Because of that, I knew as soon as I could repair a few finances, I wanted to immediately donate a lot of money where it was needed. Then I got lucky: the pandemic placed a halt on paying my federal student loans back. Combine that with a salary bump, and I had a lot of money to do good with.

In 2018-2019, I started regularly donating to non-profits for the first time. I focused a lot on organizations in Kansas City (my hometown) as well as Pittsburgh in both tech spaces and LGBTQ spaces. I shifted dramatically this year to reduce those a bit and spend a LOT more on local food banks and organizations to help incarcerated people. I am presently donating over $250 a month to organizations all over and hope to bump that up more in 2021.

<figure style="float: right; display: inline-block; width: 250px;">
<img src="/assets/images/2021/01/sarah_and_car_title.png" alt="Car title for the first car I paid off" /><figcaption>Car title for the first car I paid off</figcaption></figure>

From the freezing of loan payments and interest on the federal student loans, I got lucky enough to find that despite paying all bills, throwing money into a savings account and into an IRA, and paying off the credit card almost immediately, my checking account was still growing fast. I started throwing money at my car payments. I paid off my car loan! It's the first time in my adult life I can say _I_ paid off a loan, but also did it a year and eight months early (saving an estimated $1800)! Combining that with a lowered insurance cost, there will be more money available monthly. I can focus on donating more and paying more to student loans.

## 2021 Goals...?

2021... I want to say it's going to be a better year. But we also said that [2019](https://duckduckgo.com/?q=2019+worst+year+ever) was the worst year and 2020 would be better. And [2018. And 2017. And 2016. And 2015.](https://www.newsweek.com/worst-year-ever-529458). And... well, yeah.

Like I said at the beginning, 2020 was a weird mix of some good things on top of a collectively terrible year. I have no specific goals for 2021. But I do know the general direction I want to go. I think I can summarize it like this:

* **Be good to myself.** (Exercise, take care of my health, keep fixing my beat up millennial finances, practice good self-care, etc.)
* **Be good to others.** (Donate to non-profits, be a good ally, help my friends, wear a mask/wash my hands/etc. so I don't catch and spread the pandemic, get the vaccine when it's my turn, etc.)
* **Try to make the world a tiny bit better.** (Be a positive influence on younger generation, vote for responsible people, leave society and Earth a bit better than I found it, etc.)

~ Sarah