---
title: "I've Hit Community Burnout"
excerpt: "At the last half of 2020, I started live streaming on Twitch. One of the popular things is for Twitch streamers to have their own Discord servers. [...] But the point of this blog post is to focus on an issue I see brewing for me: Community Burnout. "
date: 2021-01-18T12:28:00-05:00
permalink: /2021/01/18/2021-01-18-community-burnout/
image: /assets/images/2021/01/flickr_burnout_dennis_skley.jpg
author_profile: true
toc: true
categories:
  - Mental Health
tags:
  - burnout
  - communities
  - mental health
  - online communities
---

<figure>
    <img src="/assets/images/2021/01/flickr_burnout_dennis_skley.jpg" alt="Image of 4 matches against a white background. One of the matches is already burnt." />
    <figcaption>Photo by <a href="https://www.flickr.com/photos/dskley/14692471997/" target="_blank" rel="noopener">Dennis Skley</a>.</figcaption>
</figure>

**[UPDATE]** Added a section at the end for Discord server admins to add roles effectively.

At the last half of 2020, I started [live streaming on Twitch](https://www.twitch.tv/geekygirlsarah). As I started 
getting into it, I was looking at other streams and meeting people on there. I met a good number of people who I would 
call friends now, which is great!

One of the popular things is for Twitch streamers to have their own [Discord servers](https://discord.com/). If you're 
not familiar with Discord, I'd say it's like [Slack](https://slack.com/) but geared towards gamers and communities. If 
you're not familiar with Slack either, I'd say it's a chat room tool that offers you a bunch of different virtual rooms 
(called "channels") that you can discuss various things in. The nice thing about Slack and Discord over older systems 
like IRC is that they're a lot easier to visually communicate things. You have markup (e.g. _italics_, **bold**, and 
adding links), images, often get previews for online documents and other things like that. The tools are great.

But the point of this blog post is to focus on an issue I see brewing for me: Community Burnout.

## What Do You Mean by Community Burnout?

Thanks, I'm glad you asked. It's a term I'm making up (that may or may not have some real life similarity) in which you 
become a part of too many communities and your involvement in all of them decline.

<figure>
    <img src="/assets/images/2021/01/dmv_omaha_ne.jpg" alt="Image of the inside of a Sarpy County, NE DMV office." />
    <figcaption>Image of the inside of a Sarpy County, NE DMV office. Photo by 
    <a href="https://omaha.com/news/metro/omaha-area-dmv-offices-are-closing-but-county-offices-will/article_6177c110-844b-516d-ad0a-a277dba1e2ac.html" target="_blank" rel="noopener">
    Kent Sievers of Omaha World-Herald</a>.</figcaption>
</figure>

It's similar to a thing I realized at the DMV once. DMVs, doctor's offices, and other places like to print out a sign 
to aid as a way to help direct people or tell people some important piece of information. The problem becomes that they 
tend to post SO many signs that the effect means all the signs are drowned out and no one reads any them.

## First, Too Many Servers

How does this work on Slack and Discord? Let's look at examples of some I belong to. For Slack, I have an account on at 
least 60 servers. They include:

<figure>
    <img src="/assets/images/2021/01/sarahs_slack_and_discords.png" alt="Image of almost every Slack and Discord I'm a member of." />
    <figcaption>Image of almost every Slack and Discord I'm a member of.</figcaption>
</figure>

* Local communities (relative to my past and present cities)
* Online communities (like a women in tech or people that do public speaking)
* Conference-based ones (like one for each year of Strange Loop, one for all of THAT Conference)
* Ones related to a particular project (like for me some software project)

For Discord, I am a part of probably about 30 now. There's similar but different types, notably:
* Local communities (like above)
* Online communities (like above)
* Twitch streamers (one streamer has a Discord for the stream, the streamer, and the viewers that frequent it)
* Friend collectives around a game (like for people to play [Among Us](https://twitter.com/AmongUsGame) 
  together)

While many of these separate servers offer very different sets of people with separate sets of discussions, most of 
them have a LOT of the same types of channels. To categorize some:
* Pet-related (`#pets`, `#cats`, `#dogs`, `#furry-friends`, `#pet-photos`, `#all-the-pets`, etc.)
* Job-related or ways to make money (`#job-board`, `#job-search-support`, `#marketplace`, `#services_selling`, etc.)
* In software ones, ones related to tech things (`#java`, `#cpp`, `#python`, `#ruby`, `#programming`, 
  `#functional-programming`, `#ux`, etc.)
* Especially on Discord, self-promotion channels (`#live-now`, `#share-your-work`, `#special-streams`, 
  `#ads-and-surveys`, `#gamedev-art`, etc.)
* Random, generic channels (`#random`, `#general`, `#chat`, `#off-topic`, etc.)

And they're not on a few servers. I'd say on every server, Slack or Discord, I have most of the above categories.

## Engagement on a Server

So, let's say my cats do something adorable:

<figure>
    <img src="/assets/images/2021/01/sarah_and_theodosius.png" alt="Image of Theodosius sleeping on my left side while I'm sitting on the couch." />
    <figcaption>Theodosius decided to curl up and sleep on me on the couch last week.</figcaption>
</figure>

Great! I love sharing my adorable, giant cats. Where would I post this?
* There's the [Women In Tech Slack](https://witchat.github.io), probably my most-used server. I'd post in `#pets`.
* There's the [Code & Supply Slack](https://codeandsupply.co/chat), a local community to me. I'd post in `#pets`.
* I have a few friends' Discord servers. I'd post in their `#pet-photos` or similar.

After a bit this gets exhausting. I don't want to post one picture in 10+ places. There's not really a good way to 
automate this either. On my phone, I'm basically clicking the "Share" button 10+ times. On the computer, I can 
drag-and-drop that photo onto every channel (faster, but still obnoxious).

That's a silly example. Let's say I want to have a more serious discussion. Something mental health-related.

* We have `#mental-health` on Women In Tech Slack
* Also `#mental-health` on Code & Supply Slack
* Also `#mental-health` on [@whitep4nth3r's Discord](https://www.twitch.tv/whitep4nth3r)
* I recently joined [@jennyfurhh's Discord](https://www.twitch.tv/jennyfurhh) and it's all about mental health so 
  there's many channels to discuss there
* Girl Streamers' Discord also has a `#serious-stuff` channel

Not all conversations are serious. What if I want to chat about fun or uplifting things?

* We have `#uplift-and-gratitude` on the Women in Tech Slack
* Or `#successes` in Code & Supply
* Or `#milestones` on Girl Streamers
* Or `#good-vibes-only` on my friend [@LA_Draws' Discord](https://www.twitch.tv/la_draws) server
* Or `#brag` on the [We So Crafty slack](https://wesocrafty.github.io/) (though people also post all of their projects 
  on the other craft channels)

(I generally think the more uplifting channels, the better, but it's still contributing to the clutter.)

So... different avenues of conversation are plentiful. If I'm super extroverted-feeling that day, I could try to post 
something to multiple channels. But I'm often not. So it becomes a choice: Which one do I do?

## Notification Hell

There are [studies now showing how mentally draining constant notifications can be](https://www.wired.com/story/history-of-notifications/). 
While I can log on and off of Slack servers as I want on my phone or computers, you are either have Discord's app open 
(and therefore every server) or you close it (and have no servers).

Within both Slack and Discord, you can set server-wide notifications, usually notify of every message, notify of 
mentions (like someone says `@geekygirlsarah` or `@here`), or no notifications. This is a good start, but it's not 
enough.

And withing both Slack and Discord, you can set channel-based notifications. This is where it gets complicated. The 
default is to get all notifications for all channels. If I added them up, that's thousands of channels for me. This is 
WAY too much. While I can customize Slack's noises per server, everything on Discord sounds the same. Throw in that my 
roommate also uses Discord a lot and notifications get very confusing.

I've started very liberally applying channel mutes to every Discord channel I don't want to hear from and most noisy 
Slack channels. This helps a lot. Discord _still_ shows me numbers for muted channels though. So essentially I _am_ 
notified despite setting it to not do it. This usually happens because Discord owners LOVE doing `@everyone` or `@here` 
on theirs, which notifies everyone on the server of something. Often it's a "hey, I'm now streaming on Twitch". Which I 
don't really need because I get notifications from Twitch itself. 

It's just... a LOT. The result is that I want to mute everything, which is effectively like me plugging my ears to 
entire communities, which goes against what I feel it should be like.

## I'm an Extreme Case

I get I'm probably not the typical person. I AM involved in a lot of organizations and groups. I regularly talk to a 
LOT of people I've by the nature of those connections, I want to find ways to keep chatting with them. It's almost 
always easiest to join their Slack/Discord.

I'm aware that most people aren't this engaged. You might be in 1-2 local communities and that's it. Maybe 1-2 online 
ones. And maybe you only drop into a few streamers so Discord isn't so noisy. I entirely get this and that's entirely 
cool. Probably better that way.

For new communities I'm joining, I'm at a point where it's hard to feel included in them. I have the few I've been a 
part of the longest, but it's getting harder and harder to really feel like I belong to the new ones. They feel like 
they're more work than is worth it.

In general, I'm burning out. There's TOO many things to be a part of. And it's only gotten worse during the pandemic. 
People WANT to create those online alternatives to what they had in person but don't have anymore. You can see this as 
Twitch reports millions more hours of streams watched since the pandemic started. Don't get me wrong, _**I**_ want 
those replacements for the in-person things I miss too. But I'm hitting a point where it's not sustainable anymore and 
I'm losing any motivation to be a part of any of the groups.

## So, Where Do We Go From Here?

Good question again. I don't know that I have a great answer. I have a few ideas though.

<figure>
    <img src="/assets/images/2021/01/mission_vision_statements.png" alt="Sign outside of a school showing their vision and mission statements." />
    <figcaption>Vision and Mission Statement sign for Richmond Primary school. Photo by 
    <a href="https://www.flickr.com/photos/mikecogh/42275800344/" target="_blank" rel="noopener">Michael Coghlan</a>.
    </figcaption>
</figure>

One of the things that I've learned from working with non-profits is that you really need a mission and a vision. The 
mission is "This is why we exist" and the vision being "This is what we see happening." These _can_ change over time, 
and I'd say that they _should_ change in order to stay relevant to the people that the non-profit is serving.

Applying this idea to communities, I'd then ask: Who are the members? Why are they there? What's the point of this 
group? What are you, the leader of the community or a member of the community, doing to help serve that point of focus?

For example, the Women in Tech Slack offers to be a source of support for, well, women in the tech industry. This can 
be through things like answering questions about various technologies, helping women get jobs, helping support when the 
industry is being rough on them. It's also turned into a source of women to talk about things that tend to face women 
as a whole like getting appropriately paid, workplace discrimination, and more. But as we've grown, we've found there's 
room for things like channels about relationships (romantic, friendships, or otherwise), connecting across cities (many 
channels like `#loc-pittsburgh` for example), and looking at finances, home ownership, and other things too. It's 
blossomed as a great place to be but also many channels are regularly having conversations related to these topics. 
It's been a massively great support for me in the 5 years I've been there.

At the same time, it's grown to over 8,000 people now. People are joining all the time. Most people drop into 
`#introductions`, say a spiel about who they are and where they're from, then... nothing. We almost never hear from 
most new members again. I can't blame them... it's huge and it's often hard to figure out where to go. To add to that, 
most "women in tech" communities are just empty and quiet and just mostly self-promotion pools. (As in people going "I 
made a Youtube video where I..." or "I was on a podcast where I talked about..." with almost no conversation.) We've 
started to see that too on the Women in Tech Slack. People come in and most go quiet, but there are some people that 
are always making blog posts or Youtube videos are dropping their stuff in `#main` and not in any relevant channels. 
They don't talk or converse, just drop their stuff and leave until the next time they do it again. It's hard to join in 
to something so big and feel like you belong.

## How Do We Fix This?

Here's what I'd suggest:

If you're a **general, wide-topic community** (like "women in tech"), find your focus. What do you offer? What makes 
you different than any other group of people? How should people get involved or engaged in that? Focus on building that 
up the people in that group in the ways they need to be built up and supported.

If you're more of a **niche community** (like followers of a specific Twitch stream), what is your niche? Who are the 
audience or members? What are you offering them? Take those ideas and try to stay with that realm. Don't try to be too 
much. I know it feels nice to want to try to provide that, but I've seen a lot of niche communities with a ton of 
channels and so few of them have any conversations in them. Or channels that just really don't belong. Delete the extra 
things and be good at your niche. Let people get the other things from other groups.

The **larger your community is**, the more people will be lost when joining. You should have some form of onboarding at 
that point. A new employer will guide you on how to fill out HR paperwork, set up a computer, and tell you how your 
projects work. So your community should have some way (process or people) to help explain the rules, tell about the 
culture, and point them to the right places to do things and how to be involved. (This is something I see most 
communities need help with.)

If you insist on **lots of channels on Discord**, then make _very_ liberal use of role-based channels. Use the variety 
of bots out there for people to add themselves to roles to unlock channels. Allow people to opt-in to the ones they 
want based on games they play, discussions they want to have, happy/sad/serious/etc. conversations, and whatever else.
It benefits everyone, not just the person opting in as people can tell who _wants_ to be there by looking in the 
member list.

Finally, **a community is about the people involved**. While it might be focused around some topic, trait, person, or 
thing, it really doesn't work unless it focuses on the people that are there. I've seen communities regularly focus on 
discovering what people need/want and aim to provide that, but also some communities that never do this (like leaders 
usually doing whatever they want for example) that the community just doesn't really work. And that just ruins the 
community.

A plethora of communities aren't bad, but a plethora of communities that have too many random notifications, too many 
channels that aren't necessary, no engagement, and/or no focus are a cause for burnout.

I've hit community burnout, and it's time for me to start figuring out what communities I need to leave so I can be a 
valued member of any of them.
