---
title: "The Most Important Things I've Learned About Software Management from My Developer Career So Far"
excerpt: "I started my first programming-related job in 2005. Since then, I've had 13 managers across 10 teams (not including any contract work). It's a lot. I've definitely had a variety of amazing, wonderful managers I'd drop a job in a heartbeat and go work for. I also have had managers I quite frankly hope I don't run into again. Through these experiences, I've learned a lot about what I think makes up great management, both in terms of how a team can be managed and traits I believe a great manager has." 
date: 2021-06-22T00:00:00-04:00
permalink: /2021/06/22/the-most-important-things-ive-learned-about-software-management/
image: /assets/images/2021/06/woc-in-tech-team-meeting.png
author_profile: true
toc: false
categories:
  - Career
tags:
  - career
  - management
  - teamwork
---

<figure>
    <img src="/assets/images/2021/06/woc-in-tech-team-meeting.png" alt="Picture of 5 women sitting around a conference room table discussing something at a meeting." />
    <figcaption>Photo by <a href="https://www.flickr.com/photos/wocintechchat/25772186816/in/album-72157665958495865/" target="_blank" rel="noopener">WOTinTechChat.com</a>.</figcaption>
</figure>

I started my first programming-related job in 2005. Since then, I've had 13 managers across 10 teams (not including any 
contract work). It's a lot. I've definitely had a variety of amazing, wonderful managers I'd drop a job in a heartbeat 
and go work for. I also have had managers I quite frankly hope I don't run into again. Through these experiences, I've 
learned a lot about what I think makes up great management, both in terms of how a team can be managed and 
traits I believe a great manager has.

I've taken a lot of my experiences and tried to boil them down into 15 points. (For the sake of not 
calling out any manager or any company, I am anonymizing any specifics that may come up. I'll refer to all jobs, including 
my present one, as a past job for the sake of this article.) 

### 1. Managers should be "employee advocates", not "task pushers"

Before I got into tech, the managers I had for food or retail jobs were people who told you what to do next. There were 
lists of stuff to do, and they assigned you a thing to do.

The best managers I've run into in tech have been the opposite. They've set up an environment that self-selects into 
tasks. The manager then becomes the person that does whatever they can to help you succeed. If you're stopped by
a production issue, by another team delaying something you need, or by unclear expectations of what to do, the manager 
should be the one that can spend the time clearing up those delays so that you can get back to work. They also work 
_with_ you to reduce anything that's delaying your productivity.

This is why I call them "employee advocates." They're there to help me succeed on my tasks, not boss me around with 
what tasks to do or how to do them.

### 2. Managers should be good at organizing projects/tasks/etc., but more importantly better at organizing people

I remember one time when a manager mentioned something about a disagreement with a coworker in my 1:1 with them. They 
had noticed that we were really getting on each other's nerves, which was really disrupting us both. We talked about it, 
and I felt better after that meeting knowing what I could say to my coworker to maybe reduce our stress. We went for a 
walk, talked it out, and in the end, we actually became friends after we weren't on the team anymore.

I've also had one team where, after not really clicking with a coworker and mentioning this to my manager, realized 
they probably had no clue, and in fact seemed thrown off at me mentioning this. In fact, they thought this person was 
not just smart but extremely cooperative. I learned later from other coworkers that they felt about the same as I did.

I think it takes a particularly keen sense of understanding people to be a great manager. There are lots of dynamics 
at play among people on a team, and it's not always going to go great. However, I think a great manager can help 
reduce (if not eliminate) those tension points and at least keep the team in a good working flow.

### 3. 1:1s are vital, should happen frequently and regularly, and are about you the person

One-on-ones (1:1s) are just meetings between you and your manager. I think they're important because they can fill them in on 
the things they can't see that are happening, but also fill _you_ in on what's happening that you can't see. I think 
they should happen frequently (at least once a week or every other week) and regularly (if they have to get cancelled, 
reschedule them, and don't skip more than 1). I think aiming for a one-hour time block is good, but it's ok if they 
end early.

One of my favorite questions a former manager always asked as the first thing in our meetings was "What's your stress 
level?" I think this is a great example of what a 1:1 should be: about you, the human person. 1:1s should _never_ be 
about project or task work. Why? Because literally every other meeting is about project or task work. This meeting is
about you. How are you feeling? How are you handling the workload? How are things in life going? How are you getting 
along with your coworkers? What is going really well right now? What could use improvement right now?

Heck, I even had one manager that always ended 1:1s with "What can I be doing right now to help you succeed more?" I 
often felt he was doing great, but once in a while I had a suggestion, and he truly seemed grateful to work on that. I 
keep that idea with me as a great question to ask people even outside of work.

### 4. You should always get more out of your 1:1s than your manager

Again, these are meetings for you, the human. If your 1:1 is a project check-in, the manager is doing these wrong. 
Your manager has standups, retros, planning meetings, collaboration meetings, meetings with product owners, and more to 
get important updates. This meeting is for you.

What should you get out of it? I like the model one particular manager used:

* What is going well for you right now?
* What could use some improvement for you right now?
* What am I (manager) doing well right now?
* What can I (manager) do to improve right now? 
* What can I (manager) help do to help you succeed/reduce impediments?

While these could be work related (ex: "My tasks on Project A are going really well right now"), I also think they can 
be outside of work too (ex: "I just got accepted to speak at a conference and I'm excited!"). I like this framework 
because it's focused on the manager being that "employee advocate" (remember point 1?).

If you're not getting anything out of these meetings, it's probably a project update meeting and not a 1:1.

### 5. The career ladder shouldn't assume you go from an individual contributor to management

I had a manager once mention to me in our first 1:1 that they wanted to do check-ins, but also think long term about 
my goals. This could be company-specific, but also just career-specific. He was even upfront... he said if this meant a 
skill I wanted to develop that I could take with me even after that job, it was still important. And this is a _great_ 
thing to ask those you manage.

But he said "if you wanted to just stay a developer, or whether you wanted to keep going into management, I'm here to 
help you think through that." And WHOA... I was thrown off. I didn't really want to go into management (at least at 
the time).

But one thing I realized after a bit is that the career ladder for growing as a contributor (developer, designer, etc.) 
is NOT the same ladder as management. They're not even tied together. They are entirely _separate_ ladders. I've seen 
developers get promoted to become managers and just plain suck at it. Remember earlier when I said how important it is 
to organize people? Some people seem clueless about how the other people think and work, and don't seem to have a 
strong awareness of those people dynamics. This assumption is very bad, and I think it's important for managers as well 
as team members to realize.

### 6. Psychological safety is extremely important

I worked at a place once where I got to try out teams before choosing a permanent one. It really was a cool idea! But 
one thing that came up for me was on my third team, I needed my laptop to be a very specific configuration, one that 
my machine didn't have from the past two teams. I had an issue come up one day, and asked my coworker. He had been 
there too long and didn't remember how to set up something and told me to go ask the most senior person on the team. I 
went to ask them about my problem, and he just laughed at me. "Why would you set it up like that?" This shook me from 
that day forward.

Psychological safety is an idea that you won't be punished or humiliated for speaking up at work, whether it's about 
ideas, questions, concerns, or mistakes.

Unfortunately, as you saw above, I've been on some teams that broke that safety for me. I'd be judged on the code I 
wrote, I'd be judged on the questions I'd ask, I'd be shot down on good ideas I had (or worse, shot down and others 
took them and they were then good ideas), and more. It not only ruins my feeling of safety on the team, but it harms 
my work and my productivity. It's an extremely strong red flag for me and often leads to me starting to look for a new 
job. 

I think managers can help foster teams that have psychological safety, and if they see something hindering it, or if 
it comes up in a 1:1, they have the power to stop it. It comes with the people organizing, and might be one of the 
most powerful things they can help do to help guide how a team works with each other. 

### 7. The manager can really help foster (or destroy) a team culture

Early advice I got for interviewing often told me to ask interviewers (often managers) about the tech stack or version 
control or other more technical things. Nowadays I never ask those types of questions. I ask all interviewers about 
team culture. It plays a _much_ larger part in how well I function on a team than the specific tech things I have to 
use.

I've watched teams go from a shiny, happy, cooperative team to a dysfunctional mess because of management's meddling. 
While psychological safety plays a large part, I think a manager can also help make standups fun with silly traditions, 
encourage group outings (if you're in-office anyway), and tell silly jokes. Why should a team be all serious, all the 
time? Some of my favorite teams of all times played pranks on each other, and even one of my managers was the biggest 
prankster of all of them. Encourage this! (Don't worry, I epically pranked him several times as well.)

### 8. Managers need to _always_ be clear on their expectations of work, contributions, team leadership, and more

I remember one project I was working on where my manager laid out the architecture of the project, laid out all of the 
cloud services it would use, laid out what I'd work on as well as everyone else, and for nearly all of the first half 
of this thing was at all meetings and led it all. They trailed off due to getting busy, and I picked up the slack on 
coordinating things with the other people involved and eventually guided the project to its end. At the end of the 
year, it was revealed to me that the leadership I showed during the second half was their expectation. This wasn't 
pointed out in the beginning, and in fact seemed almost the exact opposite in the beginning. This almost cost me a 
bonus because my yearly review didn't turn out as I expected because of this.

I get sometimes team dynamics and project dynamics shift rather quickly with regards to who is doing what and who is 
leading up some or all of the effort. However, if there's an _expectation_ of something, it needs to be very clearly 
laid out. This goes along with that employee advocate thing. Your manager isn't working with you and working to help 
you succeed if they cannot lay out the expectations of your tasks, a project, how you'll coordinate things with other 
teams, or more. I may need to ask expectations of smaller things, but large-scale expectations should be provided by 
the manager.

### 9. Yearly reviews _should_ be absolutely no surprise and _should_ be low stress

One manager told me this advice once. Sure enough, I was nervous when year-end review season came. I filled out my 
form, sent it in, he sent in his changes, then we met to go over it. The only thing that surprised me? He ranked me 
the same way I ranked myself (both positive and negative), but he ranked me better on some things than I did. I had 
no real reason to be nervous!

If you have that great relationship with your manager, if your 1:1s are focused on you (the human), and if your 
manager is advocating for you and working to help you succeed, there's _no_ reason your yearly review should be a 
shocker. There's also _no_ reason it should be high stress. Those expectations from point 8 should be clear enough that 
also aids in making reviews a low-key thing. 

Similarly, if things are going bad, then hopefully your 1:1s are a time to dive into that. Find out what's causing it. 
Your manager should be advocating to fix what's holding you back, telling (maybe even forcing) you to take vacation, 
helping reduce team stressors, and more. The company shouldn't ever surprise you with warnings about your performance 
being bad.

### 10. If you have meetings with your team, they should get something out of it.

Of _course_ all teams have standups, right? Where you meet in a room and everyone stands while they give their status 
updates about what they did yesterday, what they're doing today, and what's impeding their work. Right?

But what happens if your team is so big that there's so many pieces going on that standups become overwhelmingly 
useless? Or so small that everyone's stretched thin on their own things and it's still useless? I've been a part of 
both and honestly it always feels like a waste of my (and my team's) time.

At that point, your standups aren't there for team's benefit anymore. They're now manager check-in meetings. If 
this happens, then there's no real point to them. Give everyone 15 minutes back of their time and have them post 
updates on Slack or whatever and the manager can read at their leisure.

Ideally, meetings with your team should benefit the whole team. If they don't, don't include everyone. Especially if 
there tends to be a lot of them.

### 11. Your manager should be a buffer between developers/ICs and upper management

One of the things I've learned is that once you jump over to management world, there's a lot of politics. It kind of 
comes with the territory I suppose. One manager told me once that they thought a good manager keeps upper management 
politics out of the way of individual contributors. It lets them focus on the work that needs to be done, and the 
manager can pass down any information that the person might need to know.

Though I've also had some managers that have done a bad job of shielding me from those politics, which can get ugly and 
drain on the mental health. Similarly, if they're _too_ good at shielding and never tell you about what's going on, 
then you never really know what's happening and might be blindsided by things that affect you at the company. I've 
had both and it's not really fun either way.

Now I actually prod my managers in our 1:1s. I try to find out what's happening. Not a lot usually changes from week to 
week, but month to month might see some changes. You never really know. But it's probably better to at least try to 
understand what's happening before some giant news drops you're not expecting.

### 12. Skip level meetings should be a thing at more companies

A skip level meeting is one not with your manager but your manager's manager. I was surprised at the first company 
that had a skip-level 1:1 meeting. I think I was initially both surprised and intimidated. Surprised because I wasn't 
expecting it and didn't really know what to say, but intimidated at their title and how it felt almost scary. But they 
also were super kind and wanted to make sure I succeeded and were there to also help ensure my success.

I think this should be a thing at more companies. I think it helps diversify your input on what's going on at the 
management level. It also gives you a good connection with the manager just in case you ever did have to go past your 
manager to talk about something important. While I haven't specifically asked for this at companies that haven't 
offered it yet, I'm thinking about starting to.

### 13. You should know your team members, and spend time just hanging out with them. On the clock.

At one of my earlier tech jobs, I was still pretty new to that job when one of my teammates said "Let's go get ice 
cream!" They all thought that was a great idea and started to get up to walk out the front door. I was definitely 
thrown off. My first thought was "whoa wait... you can just LEAVE?!? And go eat ice cream!? on the clock!? And they 
don't care!?!" The answer was yes we could, no they don't, and the ice cream was great!

More important than the ice cream was that time hanging out with my team. It broke down some of that 
all-serious, all the time type of mentality that can tend to build up if you only focus on work all the time. Building 
that team camaraderie might feel nonsense or unimportant to some, but I think it really helps build a better
environment for working both independently and together as a group. When you start to understand more about your 
teammates and understand their situations, how they work, what's going on in their lives, you relate and empathize 
with them. It makes you more able to know how to work with them easier. I had one team where we bickered a lot, and 
I dragged them to the work cafeteria to just chat over cookies and snacks, and we worked a LOT better after that.

I hope the manager of the team sets up times for the team to hang out. If not, maybe consider setting it up yourself!

### 14. Your team should be empowered to make code improvements (refactors, etc.) but should also be collaborated as a team

Though someone may "own" the product, your team should "own" the code. You're the ones working in it, and you're the 
ones that will have to deal with the problems in the future. The team should be empowered to make changes to make it 
easier to deal with, well documented, tackling tech debt, refactoring things, or whatever.

The downside is that I think there needs to be a coordinated plan of attack. The code, as it is now, is what you're 
used to dealing with. If someone goes through and makes large changes to how things work, it will take time to adapt 
to understanding it and working with its new form. (For example, changing how Docker containers work, altering the 
startup script, extracting things out into libraries or replacing your code with a third-party library, etc.) If the 
team has discussed it, that transition to learning it is probably minimized. If it happens, and it's unexpected, then 
there's a huge hurdle on everyone to figure out.

Take time every sprint (or some time period) to think through changes you want to make that aren't bugs or features. 
Figure out how you want to break those down, take on some tasks, and let your team know when they're coming down the 
pipeline. It will help keep everyone at a good productive level, if not an improving productive level!

### 15. You HAVE to onboard people and do it well. (With exceptions for brand new engineering teams.)

My final tip deals with bringing on the new people. I've heard horror stories from people for years about the terrible 
experiences people have joining teams. Laptops not being ready, accounts not being ready, being clueless about HR forms, 
teams not knowing a new person is coming, and more. Heck, I even [wrote a conference talk](/speaking/building-your-team-to-last/) 
that included a lot of things about how to go from "we need to hire someone" to hiring, interviewing, onboarding, and 
retrospecting on that hire.

What will help your new team member integrate in a great experience? Figure out your onboarding strategy, then work on 
it before the new teammate comes along. The more materials you prep, the more suggestions you offer on getting things 
right, and the more you get accounts and computer right, the less overwhelmed the new person will be, but also the more 
energy they'll want to bring to your team.

While some of this work may fall on the team to make sure stuff is documented, it also falls on the manager to help 
guide the process when they come in. Take all the above and use it to help shape and form that new relationship! Oh 
and make sure they don't have to call tech support to get their password reset on the first day.

## Conclusion

I never used to want to be a manager, but last year the thought entered my mind more seriously. While I don't believe 
I'll end up in any sort of lead or manager position at my current job, who knows what the future holds? I have mixed 
feelings about making the switch. But if I do, I feel I've had many wonderful (and a few not-so-wonderful) examples of 
managers to base my future experiences off of. While I would still want to take some management trainings probably, I 
think considering a lot of the above would be a pretty good framework to helping out a beginning manager. Or maybe even 
an experienced one. Even if I don't become a manager, I've known what to look for in my team's managers in interviews, and it has definitely helped shape my future interviews and what I look for out of a new position.

