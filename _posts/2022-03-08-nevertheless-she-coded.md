---
title: "Nevertheless, Sarah Coded"
excerpt: "Happy International Women's Day! This is my annual 'Nevertheless, She Coded' post!" 
date: 2022-03-08
permalink: /2022/03/08/nevertheless-she-coded/
image: /assets/images/2022/03/it-was-never-a-dress.jpg
author_profile: true
toc: false
categories:
  - Women in Computing
tags:
  - shecoded
---

<figure>
    <img src="/assets/images/2022/03/it-was-never-a-dress.jpg" alt="From the #ItWasNeverADress campaign" />
    <figcaption>From the #ItWasNeverADress campaign</figcaption>
</figure>

Happy International Women's Day! This is a part of Dev.to's trend of yearly ["Nevertheless, She Coded"](https://dev.to/shecoded) posts.
{: .notice--info} 

## My biggest technical achievements are...

On a professional level, I'm proud of getting to a point in my career where I've been known to be a great problem solver. I've had many coworkers that say I'm great at being thrown into problems and not just coding up solutions but finding creative ways to solve the problems. I also have a lot of past teammates, and even managers, that have said they've learned a lot after working with me. I'm pretty humbled to know that there's a lot of people out there that would gladly work with me again (and have even tried to drag me to their jobs with them)! 

On a personal level, I'm incredibly proud of [Code Thesaurus](https://codethesaur.us)! It's been an idea of mine that I honestly hoped someone else had made somewhere. After not finding it, I finally worked on it myself. It took several years to get myself to work on it, but now that it's alive and well, it's been amazing to see the overwhelmingly positive feedback come from it!

## I pledge to break the bias in tech by...

I try to not only shine as brightly as I can in the tech industry, but try to help other women I meet along the way shine too. I think when people look at women and assume they're not technical or can't work at the same level as the men in the same settings, they're missing out on the creativity, intelligence, and empathy that their products and teams can have. It's not always easy to try to do this, but by continuing to try to show this, either through my own work or by highlighting other women's work, it's really bettering the tech industry as a whole.

## Throughout my career (as a software developer, in tech, etc), I have overcome...

I have overcome a lot of bias in my jobs, in job interviews, as a conference speaker, and probably other areas. I'm reminded of some jobs where senior level developers were surprised to learn things from my junior level self, or where I had to fight to have my great ideas listened to. I'm reminded of job interviews where they assume I really should be a teacher instead of a developer because I teach and mentor alongside my jobs. I'm reminded of conferences where I walk in the door and people are shocked that _I_ was the one that gave the most popular technical talk at the event.

I also realize I've overcome a lot of bad team cultures and a lot of burnout as well. It's easy to go into a job all bright-eyed and bushy-tailed, but it's hard to stay there when the stress piles up, when managers don't listen to you calling out problems, when you get in trouble for missing deadlines that are a result of other people not delivering. It's exhausting. I have no wise words to get around this aside from just keep pushing on and maybe you'll come across a better place where this isn't the case.

## Advocating for myself looks like...

Advocating for myself is hard to describe sometimes. It sometimes means responding to the men in the room that say the idea in the meeting you already said with "Wow, I'm glad you liked my idea!" It sometimes means taking mental health days from work because it's too much. It sometimes means having backchannel conversations with all of the other women in tech you know to ensure the situation you're in is real and you're not overreacting to something. It sometimes means quitting a job before a new one is fully lined up because you can't deal with the toxicity of your job anymore.

## I pledge to support women, non-binary folks, and other minorities in tech by...

I will continue to support the underestimated people in tech by offering my advice for free to them. I'll continue to mentor them when I can. I'll continue to offer free resume reviews. I'll continue to help them polish and refine their conference talk proposals (even if it means we're fighting for the same spots at an event). I'll continue to mentor the middle and high school students by teaching programming and robotics. I'll continue to be a part of every industry leader panel I can be a part of for high schoolers, bootcamp students, university students, or whatever. I'll continue to follow, retweet, and boost other underestimated people in tech on Twitter with whatever platform I have at the time. And I'll continue to donate money to related non-profits to help them succeed.

## I’m excited about...

I'm excited to see the all-girls robotics competition team I mentor continue to get great media coverage, and hopefully win more awards this year. 

I'm excited to see how Code Thesaurus has grown by the end of this year. 

I'm excited to see how the women I've mentored in the past week get over the career hurdles they're currently at. 

I'm excited to see how I grow and evolve at my current job, and to see where I might go (whether as a developer, moving to management/leadership, or what new projects I land on).

I'm excited to see what the next events or conferences I'll be able to keynote are.

I'm excited to see what other random things pop up in my life, and on a whim I decide to go with it, and see how it continues to help me be a badass in the tech space!

