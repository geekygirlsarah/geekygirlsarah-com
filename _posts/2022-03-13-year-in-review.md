---
title: "Reflections on the Past Year"
date: 2022-03-13
excerpt: "Another year has passed. Another long year. Even though the pandemic is still messing with everyone's lives (including mine, and why I'm putting this out finally in March), I think a lot of good things have happened this year and I'd like to take my usual annual time to go back over it."
permalink: /2022/03/13/reflections-on-the-past-year/
image: /assets/images/2022/03/fully-vaccinated-tracey-sarah.jpg
author_profile: true
toc: true
categories:
- Accomplishments
tags:
- career
- Code Thesaurus
- finances
- job hunting
- live streaming
- new job
- open source
- pandemic
- retrospective
- year in review
---

<figure>
    <img src="/assets/images/2022/03/fully-vaccinated-tracey-sarah.jpg" alt="My 
    friend Tracey and I taking a silly picture in my car after we both were fully vaccinated." />
    <figcaption>My friend Tracey and I taking a silly picture in my car after 
    we both were fully vaccinated.</figcaption>
</figure>

Another year has passed. Another _long_ year. Even though the pandemic is still 
messing with everyone's lives (including mine, and why I'm putting this out 
finally in March), I think a lot of good things have happened this year and I'd 
like to take my usual review time to go back over it.

## Acknowledgements

<figure>
    <img src="/assets/images/2022/03/land-acknowledgement-pittsburgh.jpg" alt="A 
    map of the region around what we now call Ohio, Pennsylvania, and West 
    Virginia." />
    <figcaption>A map of the region around what we now call Ohio, Pennsylvania, 
    and West Virginia. From <a href="https://native-land.ca/" target="_blank" 
    rel="noopener">Native-Land.ca</a>. Pittsburgh is where the three rivers 
    meet in the center.
    </figcaption>
</figure>


In addition to the terribleness of the pandemic, 2020 brought on a wave of 
protests after the police killings of George Floyd, Breonna Taylor, and 
hundreds of other Black people that year. Then in 2021, it got quiet again. 
Society stopped talking about them. So I want to take a moment to say a few 
more things.

<a name="footnote-return" />Black lives matter. (Still.)

Stop Asian hate. (Still.)

LGBTQIA rights are civil rights. (Still.) 

I am writing this from Pittsburgh, Pennsylvania, United States, but really is 
native lands owned by the [Shawnee Tribe](https://www.shawnee-nsn.gov/history) 
and the [Osage Nation](https://www.osagenation-nsn.gov/) and pushed out by 
White[*](#footnote), English settlers.

I live in a country that is built on numerous systems that promote the value 
and lives of White people and continues to hurt the many BIPOC, LGBTQIA, poor 
and middle class, and other groups of people.

I also acknowledge that I am a White person that, as of the time of this 
writing, has a six-figure income in the tech sector. I believe that because of 
this, I need to use this privilege for good, by helping those less fortunate 
and by boosting voices of those that need to be heard.

## Pandemic

<figure>
    <img src="/assets/images/2022/03/vaccine-shot-two.jpg" alt="Photo of me 
    getting the second COVID-19 vaccine shot." />
    <figcaption>Photo of me getting the second COVID-19 vaccine shot.
    </figcaption>
</figure>

The vaccines for COVID-19 came out early in 2021, and it was quite chaotic, but 
I did manage to get my two shots in late February and mid-March. After both of 
us were fully vaccinated by the end of March, I went and saw my friend Tracey, 
and we shared a wonderful, big mask-free hug! And in December, I got my booster 
shot. As of the time of this writing, I have showed no signs or symptoms of 
having had COVID-19 (though have not been tested for it aside from having 
negative antibodies from blood donations in 2020).

## Career

At the start of 2021, I had just come off an extremely stressful and overdue 
project at work, I quickly took a week off for vacation as soon as I could. I 
didn't go anywhere, just mostly recouped at home for a stay-cation. Though 
quickly after that I reentered some stressful times at my job and burnt out 
again.

In April, I applied (again) to [18F](https://18f.gsa.gov), a technology and 
design consultancy for the U.S. Government, within the US General Services 
Administration. It was my third attempt to get in as the previous two times 
either my resume expired (as they can only consider a candidate for six months) 
or the filled the position and I never heard back. I applied on a whim, not 
even sure if I'd hear from a person this time. But I got lucky! I not only 
talked to a recruiter there, but by end of June I was already given a tentative 
offer pending a security check! Then about two weeks later, I got my final 
offer! (Hilariously, HR woman hadn't seen a security clearance clear that fast 
before. She ended up calling the security offices to confirm that was accurate!)

I gave my two weeks' notice at the old job, then took five (!!!) weeks off 
between jobs. It was a weird combination of knowing I needed the time to 
recover from burnout, feeling like I made a mistake and should have immediately 
started a new job, and also feeling really lucky for the first time in my life 
I managed to have plenty saved up in a fund to pull this off. But after the 
anxiety-filled first week of no job, I recovered amazingly well. I was happy 
and ready for the job to start!

On August 30, 2021, I was sworn in as a public service employee.

And wow! I knew from several friends at 18F over the years that the culture was 
a wonderful place to be, but actually _being_ there? I've definitely had days I 
had to really remind myself "Yeah, you work here now, Sarah." As I like to tell 
my friends, they take their work seriously, but they don't take themselves 
seriously. This comes out in everything from all-hands meetings (where they end 
with jokes and take time to show off pets, babies, and other cute things), 
virtual "coffees" (where we're encouraged to actually take 30 minutes here and 
there to just chat with friends, have book clubs, etc.), to having _very_ 
public discussions about things agency leaders have said and discussing our 
feelings about them, and so much more. And to also say I get to share my work 
and contribute to our resources that are all open source is a great way to show 
that my impact here can have a lasting effect for years to come!

## Code Thesaurus

<figure>
    <img src="/assets/images/2022/03/code-thesuarus-contributors.jpg" alt="A 
    collage of the 82 individuals that have contributed to the Code Thesaurus 
    project." />
    <figcaption>A collage of the 82 individuals that have contributed to the 
    Code Thesaurus project</figcaption>
</figure>

[Code Thesaurus](https://codethesaur.us), my open source polyglot developer 
reference tool, turned one year old! (At least in terms of being a 
production-ready site.) For someone who has a tendency to start a _LOT_ of 
things and never finish them, it's been amazing to watch this site not only go 
live, but to actually be something I'm proud of continuing to work on 
throughout the year!

[Hacktoberfest](https://hacktoberfest.digitalocean.com/) is an annual 
celebration of open source software, and they encourage people to make four 
pull requests (or code/documentation updates) to participating projects. I, 
again, spent my efforts on working on Code Thesaurus in 2021. Early in the 
month of October, someone discovered my site and shared it with the 
Hacktoberfest Discord server. Overnight, seven people claimed some issues. While 
Hacktoberfest 2020 gave Code Thesaurus about 15 pull requests, 2021 gave it 
over 90! And I personally did 10, which got me my free t-shirt and swag.

Currently, there are 20 programming languages you can compare on there in 8 
different concept sections. I'm always looking to grow the site even more and 
fill in more gaps. I also imagine Hacktoberfest 2022 will have even _more_ 
activity, so one of my goals before October is to try to automate a lot of the 
tasks that I do manually to help optimize my time, and maybe find a second 
person that could help me maintain the code repository too.

## Public Speaking

<figure>
    <img src="/assets/images/2022/03/bangbangcon-twitter.jpg" 
    alt="The organizers of !!Con 2021." />
    <figcaption>The organizers of !!Con 2021. From the 
    <a href="https://twitter.com/bangbangcon/status/1396182173980323843/photo/1" 
    target="_blank" rel="noopener">!!Con
    Twitter account</a>.</figcaption>
</figure>

I didn't end up doing any conference speaking in 2021. It's a bit of a surprise 
to me when I think about it. However, I did still speak at multiple other 
virtual events throughout the year, including a couple of keynotes. I also was 
a part of organizing [!!Con 2021](https://bangbangcon.com/), which remains my 
number one favorite conference of all time (attending, speaking, or organizing)!

For 2022, I'm not sure how much I want to submit anywhere. I am still a bit 
cautious about traveling, and I'm still trying to work out my feelings on how 
much time, energy, and work PTO I put in towards speaking versus the payoff I 
get out of it. (For example, I have several friends that use conference 
speaking as a way to put their name out there as they do self-employment or 
consulting, and this is an important payoff for them in the end.) While I love 
seeing my old friends and making new ones, while I still love sharing what I 
learn, as long as I continue to have to take time off of work or continue to 
have to pay some of my own way to attend these events, it gets harder for me to 
justify the end benefit to myself. (I do want to write a blog post on all of 
the events I've attended and break down the time, energy, and money I've spent 
attending them and whether the end result is positive or negative.)

I do believe I want to continue to submit to my top 5 favorite conferences, and 
will still consider all events I'm invited to speak at.

## Live Streaming

<figure>
    <img src="/assets/images/2022/03/twitch-geekygirlsarah-laylacodesit.jpg" 
    alt="My friend Layla and I live streaming on Twitch together." />
    <figcaption>My friend Layla and I live streaming on Twitch together.
    </figcaption>
</figure>

One of the things I've tried to do is stream some of my development work on 
Code Thesaurus on Twitch. (Believe it or not, there's a whole niche community I 
call "Dev Twitch" of people that work on software.) I started doing it partly 
as an accountability method (as in: if I can reliably dedicate time to 
streaming, it means I'm dedicating time to working on side projects). I also do 
it because a lot of people have talked about how helpful it is to see how I 
think through code problems, being able to share my experiences, and also I 
think it helps to see other women developers out there being vocal about things.
It's something I continue to want to do.

Through a variety of stressors that came up through work and life, it was hard 
to reliably dedicate time to doing it as my schedule often frequently changed 
or meant canceling streams. Also a lot of burnout meant losing the energy to go 
through with it on a regular basis. Despite this, I did manage to stream a good 
amount.

I streamed 94 hours and 33 minutes over 2021. I doubled the number of followers 
I had, and average about 12 viewers per stream. I also made $257.95 through 
that. (It comes from a combination of ads, subscriptions, and "bits", the 
Twitch currency.) I told myself that I wanted Twitch to be fun and not an 
income source, and feel that if I do it for the money, I'll eventually lose the 
joy of doing it. Therefore, whenever Twitch sends me a payout, I donate it to 
charity.

## Mentoring

<figure>
    <img src="/assets/images/2022/03/gos-practice-field.jpg" 
    alt="A photo of Maggie and I, both mentors for the Girls of Steel FIRST 
    Tech Challenge team, at the practice field assembling it for the 2021-2022 
    competition." />
    <figcaption>A photo of Maggie and I, both mentors for the Girls of Steel 
    FIRST Tech Challenge team, at the practice field assembling it for the 
    2021-2022 competition. I'm happier than my face appears.</figcaption>
</figure>

I've mentored various groups over the years, but the one I've most recently 
been involved with has been a FIRST Robotics team in Pittsburgh. The [Girls of 
Steel](http://girlsofsteelrobotics.com/) is the city's only all-girls robotics 
competition team that's based out of Carnegie Mellon University. It's my third 
year working with them, and the first two years were a bit chaotic because of 
the pandemic. I'm now the lead mentor for the middle school FIRST Tech 
Challenge teams. Though we've had a few delays on getting started, we did 
finally get to meet in-person again in October, and they've been building 
robots for competition. We had to shut down in January 2022 (I know, I know, 
it's outside of the scope of this blog post), but it's been great to be able to 
work with students in-person again and to be able to watch then learn and grow!

I also have been mentoring with one of the high school group's spin-off 
projects called BuzzBand. It's an arm band that young people on the autism 
spectrum can wear that through various feedback methods can help them focus 
more during exercise. They not only won awards through the FIRST Robotics 
system for their invention, they received a $10,000 grant through the 
Lemelson-MIT program to help make their idea into a real idea! It's been cool 
to watch this group really take charge on designing prototypes, talking to 
potential users and their parents, talking to inventors to learn about the 
process, and to get local media coverage about this. They even started filing a 
patent with their names on it! (And in a tiny moment of self-brag, they're 
sticking mentor names on it too!)

## Deleting Big Tech Accounts

I had a goal that by the end of 2020, I'd have deleted all of my Big Name Tech 
Company accounts and just eliminated the services or found replacements 
elsewhere.

Let's just say that didn't fully happen.

But by the end of 2021, I've deleted a lot more of them. I intend to continue 
to slowly get rid of more of those as I can, and eventually write a blog post 
on how I finally got rid of all of them and what I replaced them all with.

* **Amazon/Amazon Prime: Deleted as of early 2020!**
* **Amazon Web Services: Deleted as of 2019!**
* **Android: Still use but Google is fully stripped off!**
* **Apple:** Not deleted, no plans to delete yet.
* **Dropbox:** Not deleted but not used daily anymore and no more sensitive 
  data is on it. Do plan to delete.
* **Facebook:** Not deleted but effectively unused. Do plan to delete.
* **Google:** Deleted all but one account (for Google Voice). Do plan to delete.
* **LinkedIn:** Not deleted, but moved it mostly private. No plans to delete yet.
* **Microsoft:** Deleted all but one account. Do plan to delete.
* **Netflix: Deleted as of 2021!**
* **Skype:** Not deleted, but rarely used. No plans to delete.
* **Yahoo: Deleted as of 2021!**

## Bicycling and Health

<figure>
    <img src="/assets/images/2022/03/mapmyfitness-2021-dashboard.png" 
    alt="My MapMyFitness dashboard stats for 2021." />
    <figcaption>My MapMyFitness dashboard stats for 2021.</figcaption>
</figure>

I had an amazing 273 miles ridden in 2020 and I set a goal for 400 miles in 
2021! And I'm here to announce I rode...

[drumroll]

... 56.7 miles. I didn't really do well at all. I think a lot of life stress, 
work stress, and pandemic burnout really led me to get out of the habit of 
getting on the bike. And from that I definitely slid downhill with regards to 
my health. I do want to get back in the habit, so while I won't commit to a 
number, I'd like to at least get back to a routine of getting on the bike at 
least once a week and getting back on some trails around the area.

In other health news, I made some tweaks to my artificial pancreas settings and 
started for the first time fully "looping" (letting the device control all 
insulin without input from me on food/carbs I'm eating). I switched 
endocrinologists I use for the type 1 diabetes treatments, which has helped a 
lot as well. In the end, my last A1c (the test used to determine how well I'm 
managing the disease) returned a result of 5.2%. For context, you're considered 
"pre-diabetic" when that's above 5.7% and "diabetic" when it hits 6.5% or more. 
My first endocrinologist always told me keep it below 7% and she'd be happy, 
and as of that most recent test, I don't even appear diabetic!

## Goals for 2022

I've usually tried to set myself up with some goals, but I stopped setting 
specific ones during the pandemic. I do have a few general ones though:

* I want to get back into the routine of riding my bicycle, no matter how many 
  miles I go
* I want to get back into a routine of live streaming again, no matter how 
  often it is
* I have a few personal and medically-related goals I want to achieve
* I want to continue to teach, mentor, and share my knowledge and experiences 
  through the tech world to help others, especially the underestimated 
  communities in tech

## Conclusion

I always write these blog posts for me. I think it's helpful to remind myself 
of great things I've achieved throughout the year. The years are never perfect, 
and they're filled with a lot of bad things, especially after the pandemic. I 
also am aware that there's some privilege in some things I can able do with my 
life now. But I think through reflecting on the positive, I can help enter a 
new year reminded of what I have achieved, and can continue to try to do what I 
can to grow as a person. And who knows, maybe through sharing this on my blog, 
someone else can be inspired through this too.

Thanks for reading! Happy 2022!

~ Sarah

---

<a name="footnote" />\* I capitalize "Black" and "White" because in the US, 
racial identities are used as strong identifiers. The shared history and 
struggles of Black people means the term should be capitalized out of respect 
to the community of those people. There are conflicts when it comes to 
capitalizing "White" though. I stand by the opinion of the 
[National Association of Black Journalists](https://www.nabj.org/page/styleguide) 
who say that you capitalize "Black," "White," "Brown," or other groups when 
referring to race. I also stand by the opinion of the 
[Center for the Study of Social Policy](https://cssp.org/2020/03/recognizing-race-in-language-why-we-capitalize-black-and-white/)
who says that "To not name 'White' as a race is, in fact, an anti-Black act 
which frames Whiteness as both neutral and standard." Therefore, I will also 
capitalize it, even if it makes other White people uncomfortable to be called 
out in such a way. <a href="#footnote-return">Return to the section.</a>
