---
title: "2022 Year in Review"
date: 2022-12-31
excerpt: "Ah, it's the end of 2022. I'm sitting on my couch at home with my two cats as the window is open because it's a nice 60F outside. Because who knows what the weather is supposed to be anymore? It's also the year I turned 40..."
permalink: /2022/12/31/year-in-review/
image: /assets/images/2022/12/girls-of-steel-booth.png
author_profile: true
toc: true
categories:
- Accomplishments
tags:
- year in review
---

I originally wrote this on Dec 31, 2022. I never published it in 2022/2023 because I wanted to add images to it. I never got around to it. So I'm publishing it as-is so it can go out the door and I can blog about other things!
{: .notice--info}

<figure>
    <img src="/assets/images/2022/12/girls-of-steel-booth.png" alt="The Girls of Steel Robotics pit booth at the FIRST world championship" />
    <figcaption>The Girls of Steel Robotics pit booth at the FIRST world championship</figcaption>
</figure>

Note: I wrote this at the end of 2022, I just delayed publishing it so I could add pictures to it. It just took longer than intended! I'm publishing it belatedly in 2024.
{: .notice--info}

Ah, it's the end of 2022. I'm sitting on my couch at home with my two cats as the window is open because it's a nice 60F outside. Because who knows what the weather is supposed to be anymore?

It's also the year I turned 40. An age that doesn't feel old when I think about my friends that are 40-something, but I struggle to _not_ see myself as older than I feel I should be. If you've chatted with me, you probably know my birthday is a weird time for me. It's packed with a lot of feelings and memories, mostly around a couple of deaths, past depression, family issues, and other things. I saw the roughness of this year coming, so I tried to prep myself better this year.

I'll call this the _Year of Self Care_. It's not the year I've taken the most care of myself, or the year I've been healthiest, or the year that's been the best. But it's the year that I've probably taken more small actions to try to be nice to myself in various ways. I've tried to replace old things with newer things. I've tried to be more sustainable. I've tried to take care of more medical needs. I've even bought decorations for my home, something I'm really not great at doing. Little things that maybe are adding up.

The reflections of this year feel really centered around that. Whether good or bad, it feels like many things I've done in the end have been a result of some of the small actions I've done for myself. [Like every year](/tags/year-in-review/), I want to think on what I've done and accomplished and write about it. And hopefully in the process maybe others (like you, dear reader) maybe can get something out of this as well.

## Acknowledgements

It feels wrong to dive into this post without acknowledging several things still happening in the world. Black lives, among other minorities, [are still dying needlessly at the hands of authorities](https://www.nbcnews.com/news/nbcblk/report-black-people-are-still-killed-police-higher-rate-groups-rcna17169). We're still seeing [mass shootings go up](https://www.cnn.com/2022/11/23/us/2022-mass-shootings-tracking-second-highest/index.html), not down. We still have a hyper-charged US political environment that [cares more about calling each other names](https://news.yahoo.com/name-calling-politics-grabs-headlines-154100827.html) and serving [their own best interests](https://www.nytimes.com/interactive/2022/09/13/us/politics/congress-members-stock-trading-list.html) than taking care of their constituents. We've seen [rights taken away by the court systems](https://www.vox.com/23180634/supreme-court-rule-of-law-abortion-voting-rights-guns-epa). There's a war on the other side of the world being fought because [a dictator isn't getting his way](https://www.nbcnews.com/news/world/why-putin-invaded-ukraine-russia-war-explained-rcna16028). And, of course, entering the [fourth year of a global pandemic that's pretty much guaranteed to never leave now](https://www.popsci.com/health/covid-pandemic-fauci-advice/). It's a lot, and it's still very sad. Though most of this hasn't directly affected me, it is still happening and I still see it affecting my friends, coworkers, and acquaintances whom these events do affect directly. They are things we as a society still need to talk about, and we need to be there for the humans out there that are suffering. Please take care of yourselves.

## Pandemic

As I mentioned above, the pandemic is still happening, and I finally [caught COVID in August](https://twitter.com/geekygirlsarah/status/1557732806834339843) this year. It basically felt like cold symptoms plus really sore body aches. I feel like I got massively lucky, partly in that my symptoms were much milder than I had heard from friends that caught it before (probably because I had 3 vaccination shots when I caught it) but because I am in a category of people that could get [Paxlovid](https://www.yalemedicine.org/news/13-things-to-know-paxlovid-covid-19), the antiviral meds that pretty much _slaughtered_ this virus in my body within 12 hours. I'm not joking when I say I took off of work the day I got the meds and I worked the next day. It made no sense to me to take off when I felt nearly perfect. It was wild. But I acknowledge my luck. One of the mentors that was at the same high school robotics event I was at also caught it and didn't get Paxlovid, and it took about two weeks for him to recover.

The pandemic is _STILL_ real, people. Please get your vaccinations. Please isolate if you have symptoms. Please take care of yourselves.

## Health/Exercise

Similarly, I've finally started calling doctors to get things taken care of. I won't go into all of it, but probably the longest issue is that I've had a chronic cough for _years_. It was fairly minor but annoying. But after catching COVID, it got worse. It's rough and sometimes comes in small attacking fits. So I called a pulmonology (lung doctor) clinic. Now armed with a daily inhaler, an emergency inhaler, a nasal spray, a lung/breathing test and its results, and a chest x-ray and its results, I definitely feel very broken. And both the test and the x-ray reveal: nothing. Both show normalcy for my age/gender/weight. So I'm scheduled for a CT scan in about a week and a half. But that's the future and out of scope for this post. But the point is that I'm slowly chipping away at the health problems.

In good news, diabetic results are still fairly good. I'm thankful for my artificial pancreas still chugging away, regulating my blood sugar through automated insulin dosing. My old one did die and I had to replace it, but it was money well spent.

And in bicycling and exercise news: I did about 50-something miles last year and hoped to do more this year. Well... I did less. We'll just call it "less than 10 miles." I won't go into it, but I hope to find some routine that works for me next year to help me get moving a bit more reliably next year.

## Career

In [last year's post](/2022/03/13/reflections-on-the-past-year/), I mentioned getting hired at [18F](https://18f.gsa.gov/), a software consultancy within the government. And, generally, it's been refreshing to see what a job looks like when your work revolves around caring about humans (both users and coworkers) than it does about moving fast, gaining profit, or other tech mantras like those. For this, I'm very glad!

Though around the mid-point review (March), things went downhill fast. I'll summarize it with "lots of drama" related to misunderstood feedback. I nearly quit over it and was contemplating what non-tech industry I'd want to work in next. Things are better now, and I do want to work there longer. There's still a lot of nonsense happening (mainly around the churn of the project I'm on), but I'm here to hopefully see the project through until it's passed off to the contractors and other people that will own it.

I think a lot about my future though. I remember telling a manager in 2015 that management was basically out of the question, I was a developer deep in my heart. After last year and the last job, I started thinking more about getting out of development and into management. It's not that I don't like development (I still do), but I'm definitely finding myself drawn more to the larger scale problems (architecture and people) than the smaller coding tasks I've been on in the past few jobs. I'm not sure what this looks like for me yet, but it's something I'm definitely thinking about and trying to learn and read up on.

## Code Thesaurus

Speaking of development, [Code Thesaurus](https://codethesaur.us/), my [open source](https://github.com/codethesaurus/codethesaur.us) side project, is still moving along. While the past couple of years have been mostly adding programming language data, I think the past year has seen some of that along with newer features. There's been changes to accessibility, logging what people are searching, optimizations on the matching algorithm as well as the HTML/CSS used, and more. It's been cool to see what people are contributing to it! There were also over 50 pull requests made over Hacktoberfest this year. I also added a lot of checks into the deployment system too to help catch errors and hard-to-find issues before they go out. I'm thinking maybe in the next year it'll be time to start adding in another phase I've wanted to see on it. It's been a real learning experience to run an open source project with a lot of contributors, and I'm definitely looking forward to seeing where the future takes it!

## Public Speaking, Conferences, and the Lack Thereof

I think a lot of people know me as a [conference speaker](/speaking/) in the tech world. That was a pretty strong point of identity a few years ago pre-COVID. But travel shutdowns stopped nearly all of that for me. I did a few virtual events, but not really been back at it like I was prior to the pandemic. And with my job, I still really can't. Unless I can talk about something directly related to work, I'm kind of stuck taking my own vacation days, something I don't have a lot of. It's meant seeing CFPs for my favorite events and not applying. It's meant not really making new conference talks. And on a couple of instances, it's meant thinking I could maybe pull it off and backing out when I couldn't.

I periodically think about this identity I had as a conference speaker. I still like it. I just wonder how I feel about not doing it and the tradeoffs of my job. I've mostly liked my job, and it's been fine. I do miss this though. Mostly the community around it and learning lots of new things very quickly. While I could possibly try to get work to take me to conferences to attend, there's always been something wonderful about speaking at them, and I miss that a lot. 

For now, though, I keep it a part of my identity, though I haven't really done much of it in the past few years.

## Mentoring

Though one part of my identity I do embrace is thinking of myself as a mentor. I started mentoring the [Girls of Steel Robotics team](https://girlsofsteelrobotics.com/) back in 2019, a few months before the world shut down. It was rocky getting through virtual-only meetings because it meant no robot building. Last season we got to a hybrid season. The university we're based out of kept shutting down, and we'd have to stop what we were doing until it opened up again, but it was good to be back in person even part-time.

This season has been _entirely_ in-person which is wonderful. As lead mentor of the [FIRST Tech Challenge teams](https://girlsofsteelrobotics.com/first-tech-challenge-teams-9820-and-9981/) (which for us is 8th and 9th graders), I've tried to push for a lot of changes to the program to help it succeed better. And they're working! Our six girls (a _much_ lower number than usual) all built some basic sample bots, then we got the rules to this year's competition, and got started pretty quickly! Since early September, they've been designing, building, wiring, and programming and doing great work! We went to a scrimmage (like a practice competition) and the girls did great! Over their holiday break I'm trying to duplicate their robot so they have one robot for both teams. I'm also trying to fix any minor issues so they can focus their last two weeks on strategies and awards. I'm so super proud of them! I'm usually not one to toot my own horn, but I am kind of proud of the changes I've made. I think it's a better program because of it, and even if the teams get last place, they did some massively impressive work for just a few months!

I also got to watch the high schoolers [go to the world championships](https://girlsofsteelrobotics.com/news/2022-build-season-competition-season-update-3-worlds/) last season (in April) and I went with them! It was wild seeing my first world champs I've ever been to since starting to mentor in the FIRST Robotics system for over a decade. I'm proud of them too as they deserve it! I signed up to be a judge, so it was cool to interview these high quality teams and see what made them rock and how they approached their robot builds. Then my favorite part: watching the teams win awards! I'm hoping to keep going back. I have every reason to believe our teams can rock it again this season.

I also got lucky enough to go with the high school girls that were working on an invention side project as they [demoed it at MIT](https://girlsofsteelrobotics.com/news/girls-of-steel-inventeams-update-8-eurekafest/). We went to Boston for a few days and they got to go on tours, see robotics labs there, show off their invention, and meet other high school inventors! I feel like I just followed them around everywhere for three days, but again, I'm proud of them! And maybe it helped to vicariously live through them a bit as they did and saw some really cool things at MIT.

This year I also started mentoring more people in a career setting. I've had several video meetings and even one recurring weekly meeting with various women in getting into tech or trying to level up in their career. It feels good to know maybe I'm in a place where I've had enough successes that I can answer questions, help point them in a good direction, or just otherwise help them get where they want to be. This is definitely something I want to keep doing!

## Deleting Big Tech Accounts

I talked some on last year's post about deleting the Big Name Tech Company accounts and trying to move to different systems. There's more updates on that but I mostly won't go into it yet. But there is one I want to get into.

I finally deleted my old Google accounts. I no longer have a personal account. (I still have one through work and one through Girls of Steel, but they're tied to those email addresses and not mine.) Google Voice was basically the last thread keeping me tied to one. As well as the occasional friend that would send me a Google Doc. However, I just moved my Google Voice phone number to my phone and backed up my texts, restored the vast majority to my phone (a weird process I had to custom script), and then deleted it. 

## Self Care

Finally, back to where we started. The Year of Self Care. I don't want to say I _don't_ take care of myself, but it's easy to kind of shrug off doing good things for myself. And this year I've been trying not to do that as much. Examples include:

* Buying some things to hang on my apartment walls and things to put on furniture to decorate some
* Buying the top-rated Wirecutter-recommended office chair, since I sit there for many hours during a work week
* Slowly replacing things in my house with more sustainably sourced stuff (like reusable cloth "paper" towels, shampoo and conditioner bars, kitchen brushes instead of sponges, etc.)
* Buying my first LEGO sets as an adult, putting them together, them putting them in a display case
* Scheduling recurring virtual "coffees" with my favorite coworkers so I can just take a break from work and chat with them about whatever is on our minds at the time
* Spending time with a friend pulling all the clothes that no longer fit out of my closet (a pile so large I was dreading dealing with) and starting to buy new clothes, shoes, and a coat that did fit so I can start to feel fashionable again
* Hiring a house cleaner to come once a month. Then another one when she got a full time job somewhere else. And hopefully can maybe figure out how to get her twice a month.
* Getting my car detailed (it's _SO_ clean inside!)
* Buying new, good quality pans to cook with (and stop using the ones that are slowly faking the non-stick material off)
* Finding ways to eat out less (which sometimes means things like TV dinners if energy is low, but they're probably "healthier" than fast food)
* Taking this week off of work despite knowing I usually like working holiday weeks because they're easier, but also knowing I'm _very_ burnt out and the time off has been helpful
* Telling myself it's ok to pay for multiple streaming services, even if I'm not watching as much on them at any given time
* But also to watch a lot of shows or movies, or play a lot of video games, to unwind at the end of the day (which turns out to be good times to play with the cats, who love it)
* To buy myself a Dairy Queen ice cream cake around my birthday (though it's not a "birthday" cake as it's blank with flowers and has no writing on it)
* To claim items on Freecycle that I don't normally buy for myself (like claiming bath bombs, nice scented lotion, and a dehumidifier)
* Actually finishing the first book I've read in [insert embarrassingly large amount of time here]

There's probably even more things I haven't thought of. But I like that this is a pretty big list of small things. Things that on their own don't really matter a ton, but cumulatively can help make a difference. 

## Goals for 2023

What does 2023 look like? I don't know. I have felt like most years I go in with a sort of theme or larger goal I want done, but I don't really have one. I think I want to finish important things I started (like the medical appointments to get health answers), develop better routines (like more regularly get things done in the morning I want to), and continue to be kind to myself. I want to see my robotics girls off to competition. I want to get myself in a good place at work. And who knows what else? Nothing super specific. I think the goals are just to see I'm generally headed down a good path, and keep myself wandering that path.

## Conclusion

All this talk of self-care makes me think about my friends and people I know and hope they're taking care of themselves as well. Are you doing nice things for yourself? Are you taking care of your health? Are you going down a generally good path on your life (even if it's hard to see it or feels it's moving slow)? Please do! And if we used to talk and haven't in a while, please reach out. I spend a lot of time reaching out to friends, but sometimes it's helpful to have them reach out to me if the ball gets dropped. I'd love to hear what's new and what's happening with you.

Here's to 2023, wherever it takes us!
