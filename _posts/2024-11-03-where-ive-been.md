---
title: "Where I've Been"
date: 2024-11-03
excerpt: "I've been quiet lately. At least online. And kind of in-person too. Here's some updates."
permalink: /2024/11/03/where-ive-been/
image: /assets/images/2024/11/unsplash-estonia-zen.jpg
author_profile: true
toc: false
categories:
- Life Updates
- Mental Health
- Writings
tags:
- burnout
- career
- communities
- community involvement
- emotions
- live streaming
- mental health
- new job
- projects
- retrospective
- self care
---

<figure>
    <img src="/assets/images/2024/11/unsplash-estonia-zen.jpg" alt="Rõuge, Estonia. Photo by Kristin Wilson from travelingwithkristin.com" />
    <figcaption>Rõuge, Estonia. Photo from <a href="https://unsplash.com/photos/brown-wooden-dock-on-lake-during-daytime-S7rdY1l_YEA" rel="nofollow noopener">Unsplash</a> by <a href="https://www.travelingwithkristin.com" rel="nofollow noopener">Kristin Wilson</a>.</figcaption>
</figure>

I've been quiet lately, at least online. And kind of in-person too. Social media (including Twitter (or... nah, not going to call it that), Facebook, Mastodon, Bluesky, Slack and Discord communities) I have open on tabs but don't really read them much. Definitely don't post much. I have my website but haven't published anything since at least March 2022. I used to live stream some side projects on Twitch but stopped.

So... yeah... I've been quiet online.

I'm still here. I'm generally fine. My health is alright (minus some small ups and downs). My cats (aka my house panthers) are happy. I'm still employed and work is going good too. Just... it's all kind of average.

Why have I been so quiet? I do miss interacting with my online spaces. I miss the communities and friend groups I had. But it's been hard to get back into it.

## My online downward trend

I think it started with the pandemic. Locked up in my home. In-person communities stopped meeting. Conferences stopped. Travel stopped. _Everything_ pivoted to being online. And it was good. I saw some people more online than I did in-person before. It was kind of nice!

I moved in the pandemic from an apartment alone with a kind of sucky landlord company to a better one with a friend-of-a-friend roommate. It was... ok. We got along until we had some stressors. We neutrally parted ways when the lease ended. I moved again about 1.5 years after my last move. And I ended up alone again. I'm used to living alone as I've done it several times now, but I think during the trailing edge of the pandemic it hit a bit harder.

The other in-person activities I did were slowly stopping too. When the world opened back up, many of them stopped or I stopped going or backed away from them. Others quit for good. And others just never came back to full activity. It got harder to be engaged anywhere.

What also didn't help were my developer jobs that started off _GREAT_ but over time they got worse and harder to work at. I saw issues coming up. I mentioned them to my manager as things I think might affect me and the team. Then they did and my manager didn't have my back. Then I heard there was an opening at a place I wanted to work at for years. And I got in!

But quickly things got sour there too. The people I worked with were great! The project work was great! But my supervisor and I weren't meshing well. There was bad feedback loops happening there (with him and other places too). By the time I tried to change my supervisor, there wasn't a director anymore I could ask. It was a weird spot. For a place I heard so, so many good things about and looked forward to working at for many years, I left there much earlier than I was hoping to.

By this point the pandemic was "ending" (though COVID-19 is now here to stay), things weren't really transitioning back. The tech communities I was in were still low activity, or just stopped entirely. 4/6 of my top favorite conferences ended by 2024. I was still living alone, but seeing my friends less even when we could go back out into the world. Whats-his-face bought Twitter and ran it into the ground. AI proliferation all over made me want to use social media less and less anyway, though I had already stopped by 2023 for the most part. And my jobs sucked a ton of my mental health away and I'd do my work then just go "home" (or at least away from the computer) and just watch TV or play video games. 

## My gradual rise back up?

The rut got better. I got a job where I now am founding and running a whole department and I'm doing great (though have to admit it took about a year to really feel like I was). My main in-person side activity, mentoring middle/high school robotics teams, picked back up full swing and I mostly threw my energy at that as the original founders were retiring and I was already helping run activities that they did. I started seeing some friends in-person again (though definitely not at the rate I used to). My new job (and even the last one) didn't really allow me to go back to conferences as easily, so I was barely managing to make it to 1-2 a year instead of the 6-10 a year I used to. And my conference speaking mostly quit too. But I was able to go to a couple of the last events some of those conferences held and see them off to a happy conclusion.

Life now? It's mostly going to work at my remote job (but once every 1-2 months I head partway across the state for an on-site). I mentor the robotics kids 2-4 days a week. I occasionally see some in-person friends. I try to head to the coworking space and tech community regularly (though their physical space is shutting down at the end of the year). And I still have a core group of friends I hear from via text, Signal, Slack, and Discord, and that's been nice.

Life has just calmed down and slowed down. And I haven't really spent the same time again on online communities or social media as much since then.

## Where's that left me?

I send a preview of this post to my friend Pam. She said "it sounds like a grieving post for what you lost." And she might be right.

There were a _lot_ of cool things I did before the pandemic hit. I think I do miss them. While in a way I could get them all back, in another way it would be hard to. Some of it would mean changing jobs. Some of it would mean reviving things that have ended somehow. Some of it would mean me trying to find more energy than I've had in the past 4 years. It feels harder to shift my energy around that much, even if I am finding I'm slowly getting more of it back.

I guess I really am kind of grieving the loss of parts of my pre-pandemic life.

## So what now?

Well, I don't know. I don't know if I have much of a point to this post besides just dumping a few random feelings out.

It was fun to ask my Question of the Day on Twitter (or even starting on Mastodon). Perhaps finding a way to have it more front and center in my life might help. I miss Twitch, and maybe my energy level is picking back up again that I could regularly be back on camera and working on side projects with an online audience. With so many conferences and in-person communities stopping I've wondered if there's something I could start on my own to help fill that void. And I could try to reach out to my in-person friends more to see if they'll do more things with me.

I'm still around though. I miss a lot of my former pockets of online communities. If you've wondered how I'm doing, I'm ok. I feel like I'm slowly coming back around.

Though I'd love to hear from people I used to interact with more! Feel free to drop me a message in one of the many ways to get a hold of me. (Though I'll admit I don't really get notifications from Facebook, so that's harder.) But maybe soon you'll see me around online more.

But at least I can reset my "days since last blog post" to 0!